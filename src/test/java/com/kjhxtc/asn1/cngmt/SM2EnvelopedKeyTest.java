package com.kjhxtc.asn1.cngmt;

import com.kjhxtc.asn1.x509.AlgorithmIdentifier;
import com.kjhxtc.asn1.x509.SubjectPublicKeyInfo;
import com.kjhxtc.jce.interfaces.ECPrivateKey;
import com.kjhxtc.jce.provider.KJHXTCProvider;
import com.kjhxtc.util.BigIntegers;
import com.kjhxtc.util.encoders.Hex;
import org.junit.Assert;
import org.junit.Test;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.math.BigInteger;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Random;

public class SM2EnvelopedKeyTest
{
    static
    {
        Security.addProvider(new KJHXTCProvider());
    }

    @Test
    public void getInstance()
    {

    }

    @Test
    public void testGetInstance() throws Exception
    {
        Cipher cipher = Cipher.getInstance("SM2", KJHXTCProvider.PROVIDER_NAME);
        Cipher sm4 = Cipher.getInstance("SM4/ECB/NoPadding", KJHXTCProvider.PROVIDER_NAME);
        byte[] symKey = new byte[16];
        new Random().nextBytes(symKey);
        KeyPairGenerator generator = KeyPairGenerator.getInstance("SM2", KJHXTCProvider.PROVIDER_NAME);
        KeyPair kp = generator.generateKeyPair();
        KeyPair kp2 = generator.generateKeyPair();
        System.out.println("Recv");
        System.out.println(kp.getPublic());
        System.out.println(kp.getPrivate());
        System.out.println("Exchange Key");
        System.out.println(kp2.getPublic());
        System.out.println(kp2.getPrivate());
        cipher.init(Cipher.ENCRYPT_MODE, kp.getPublic());
        sm4.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(symKey, "SM4/ECB/NoPadding"));
        KeyFactory kf = KeyFactory.getInstance("SM2", KJHXTCProvider.PROVIDER_NAME);
        System.out.println("SEC:KEY--REAL::" + Hex.toHexString(symKey).toUpperCase());
        PrivateKey pvk = kf.generatePrivate(new PKCS8EncodedKeySpec(kp2.getPrivate().getEncoded()));

        SM2EnvelopedKey e = SM2EnvelopedKey.getInstance(
                new AlgorithmIdentifier(GMTObjectIdentifiers.id_sm4_ecb),
                SM2Cipher.getInstance(cipher.doFinal(symKey)),
                SM2PublicKey.getInstance(SubjectPublicKeyInfo.getInstance(kp2.getPublic().getEncoded()).getPublicKeyData()),
                sm4.doFinal(BigIntegers.asUnsignedByteArray(32, ((ECPrivateKey) pvk).getD()))
        );
        System.out.println(Hex.toHexString(e.getEncoded()).toUpperCase());

        SM2EnvelopedKey rk = SM2EnvelopedKey.getInstance(e.getEncoded("DER"));
        Cipher recSm2 = Cipher.getInstance("SM2", KJHXTCProvider.PROVIDER_NAME);
        Cipher recSym = Cipher.getInstance("SM4/ECB/NoPadding", KJHXTCProvider.PROVIDER_NAME);
        recSm2.init(Cipher.DECRYPT_MODE, kp.getPrivate());
        byte[] tmpSecKey = recSm2.doFinal(rk.getSymEncryptKey().getEncoded());
        Assert.assertArrayEquals("SymKey ::", symKey, tmpSecKey);
        recSym.init(Cipher.DECRYPT_MODE, new SecretKeySpec(tmpSecKey, "SM4/ECB/NoPadding"));
        BigInteger d = BigIntegers.fromUnsignedByteArray(recSym.doFinal(rk.getSm2EncryptedPrivateKey().getBytes()));
        System.out.println(d.toString(16));
    }
}