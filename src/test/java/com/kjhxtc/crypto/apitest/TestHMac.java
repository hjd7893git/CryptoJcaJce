package com.kjhxtc.crypto.apitest;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import com.kjhxtc.crypto.util.encoder.Hex;
import com.kjhxtc.jce.provider.KJHXTCProvider;

public class TestHMac {
	static {
		Security.addProvider(new KJHXTCProvider());
	}
	public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException {
		Mac mac = Mac.getInstance("HmacSHA1", "KJHXTC");
		mac.init(new SecretKeySpec("12111".getBytes(),"HmacSHA1"));
		mac.update("dsafafdasd".getBytes());
		byte[] mac_res =mac.doFinal();
		System.out.println(Hex.toHexString(mac_res));
		
	}
}
