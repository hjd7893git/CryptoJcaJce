//package com.kjhxtc.crypto.apitest;
//
//import com.kjhxtc.crypto.api.Asymmetric.BocAsymmetricCrypt;
//import com.kjhxtc.crypto.api.Asymmetric.SignOpreator;
//import com.kjhxtc.util.encoders.Hex;
//
//import java.io.*;
//import java.util.Calendar;
//
//public class ToolLicense {
//    public static void main(String[] args) throws Exception {
//
//        BocAsymmetricCrypt api = new BocAsymmetricCrypt();
//        //AsymmetricKeyPair pair = api.BOC_GenKeyPair(BocAsymmetricCrypt.KEY_SM2, 256);
//        //byte[] vk = api.BOC_GetPrivateKey(pair);
//        //byte[] pb = api.BOC_GetPublicKey(pair);
//        byte[] vk = Hex.decode("000001000000000000000000000000000000000000000000000000000000000000000000D83E43EAEF2156D6D534A03CA84EE47910EDC3BCB6DC14860FF236AE5D23BD1D0000000000000000000000000000000000000000000000000000000000000000D21B3EBDFBAA79D254CE23597DC70B8FB32C142EC86CB29257D4FA53413BD0F40000000000000000000000000000000000000000000000000000000000000000F237422F476498E815265C7CC9220C265FA9D61452526E4320DF611D7AD60492");
//        byte[] pb = Hex.decode("000001000000000000000000000000000000000000000000000000000000000000000000D83E43EAEF2156D6D534A03CA84EE47910EDC3BCB6DC14860FF236AE5D23BD1D0000000000000000000000000000000000000000000000000000000000000000D21B3EBDFBAA79D254CE23597DC70B8FB32C142EC86CB29257D4FA53413BD0F4");
//        SignOpreator sign = api.BOC_SignInit(BocAsymmetricCrypt.SIG_SM2_SM3, vk);
//        System.out.println(Hex.toHexString(vk).toUpperCase());
//        System.out.println(Hex.toHexString(pb).toUpperCase());
//
//        Calendar cale1= Calendar.getInstance();
//        cale1.set(2018,Calendar.MAY,1,0,0,0);
//        Calendar cale2= Calendar.getInstance();
//        cale2.set(2018,Calendar.JULY,1,0,0,0);
//        StringBuilder a = new StringBuilder("JNSEC.NET");
//        StringBuilder b = new StringBuilder("GUAZI.COM");
//        OutputStream buffer = new ByteArrayOutputStream();
//
//        buffer.write(long2Array(cale1.getTimeInMillis()));
//        buffer.write(long2Array(cale2.getTimeInMillis()));
//        while (a.toString().getBytes("UTF-8").length < 16) {
//            a.append(" ");
//        }
//        while (b.toString().getBytes("UTF-8").length < 16) {
//            b.append(" ");
//        }
//        if (a.length() > 16 || b.length() > 16) throw new IllegalArgumentException("Name too Lang");
//        buffer.write(a.toString().getBytes("UTF-8"));
//        buffer.write(b.toString().getBytes("UTF-8"));
//        byte[] result= sign.Signer(((ByteArrayOutputStream) buffer).toByteArray());
//        byte[] nResult = new byte[64];
//        if (null==result)throw new IllegalStateException("Sign Failed");
//        File output = new File("/home/neo/hxtc/license");
//        if (!output.exists()){
//            output.createNewFile();
//        }
//        FileOutputStream of = new FileOutputStream(output);
//        InputStream inst = new ByteArrayInputStream(result);
//        inst.skip(32);
//        inst.read(nResult,0,32);
//        inst.skip(32);
//        inst.read(nResult,32,32);
//        System.out.println("Uncut:"+Hex.toHexString(result).toUpperCase());
//        System.out.println("InCut:"+Hex.toHexString(nResult).toUpperCase());
//
//        buffer.write(64);
//
//        buffer.write(nResult);
//        of.write(((ByteArrayOutputStream) buffer).toByteArray());
//        of.close();
//        for (byte vz: long2Array(0x12345678)){
//            System.out.println(String.format("%X", vz));
//        }
//    }
//
//    public static byte[] long2Array(long a) {
//        // 0x12345678
//        byte[] out = new byte[8];
//        out[0] = (byte) (a >> (8 * 7));
//        out[1] = (byte) (a >> (8 * 6));
//        out[2] = (byte) (a >> (8 * 5));
//        out[3] = (byte) (a >> (8 * 4));
//        out[4] = (byte) (a >> (8 * 3));
//        out[5] = (byte) (a >> (8 * 2));
//        out[6] = (byte) (a >> (8));
//        out[7] = (byte) (a);
//        return out;
//    }
//}
