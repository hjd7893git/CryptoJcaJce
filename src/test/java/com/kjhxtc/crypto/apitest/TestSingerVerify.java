//package com.kjhxtc.crypto.apitest;
//
//import com.kjhxtc.crypto.api.Asymmetric.AsymmetricKeyPair;
//import com.kjhxtc.crypto.api.Asymmetric.BocAsymmetricCrypt;
//import com.kjhxtc.crypto.api.Asymmetric.SignOpreator;
//import com.kjhxtc.crypto.api.Asymmetric.VerifyOpreator;
//import com.kjhxtc.util.encoders.Hex;
//
//import java.security.NoSuchAlgorithmException;
//
//public class TestSingerVerify {
//
//	static byte[] data ="I aldfaknkn9123n 123lk1n23,ads./a".getBytes();
//	static byte[] data1 ="I aldfaknkn9123n 123lk1n23,ads./a1".getBytes();
//	/**
//	 * @param args
//	 * @throws NoSuchAlgorithmException
//	 */
//	public static void main(String[] args) throws NoSuchAlgorithmException {
//
//		sm2();
//		rsa();
//
//	}
//
//	public static void sm2() throws NoSuchAlgorithmException {
//		BocAsymmetricCrypt boc = new BocAsymmetricCrypt();
//		AsymmetricKeyPair sigkey= boc.BOC_GenKeyPair(boc.KEY_SM2, 256);
//		byte[] vk = boc.BOC_GetPrivateKey(sigkey);
//		byte[] pk = boc.BOC_GetPublicKey(sigkey);
//		System.out.println("PK:"+Hex.toHexString(pk));
//		SignOpreator s = boc.BOC_SignInit(boc.SIG_SM2_SM3, vk);
//		byte[] value=boc.BOC_SignData(s, data);
//		System.out.println("data:"+Hex.toHexString(data));
//		System.out.println("Sign:"+Hex.toHexString(value));
//
//		VerifyOpreator v = boc.BOC_VerifyInit(boc.SIG_SM2_SM3, pk);
//		int pass=boc.BOC_VerifySign(v, pk, data1, value);
//		if (0!=pass){
//			System.err.println("failed");
//		}else{
//			System.out.println("ok");
//		}
//	}
//	public static void rsa() throws NoSuchAlgorithmException {
//		BocAsymmetricCrypt boc = new BocAsymmetricCrypt();
//		AsymmetricKeyPair sigkey= boc.BOC_GenKeyPair(boc.KEY_RSA, 2048);
//		byte[] vk = boc.BOC_GetPrivateKey(sigkey);
//		byte[] pk = boc.BOC_GetPublicKey(sigkey);
//		SignOpreator s = boc.BOC_SignInit(boc.SIG_RSA_SHA512, vk);
//		byte[] value=boc.BOC_SignData(s, data);
//		VerifyOpreator v = boc.BOC_VerifyInit(boc.SIG_RSA_SHA512, pk);
//		int pass=boc.BOC_VerifySign(v, pk, data1, value);
//		if (0!=pass){
//			System.err.println("failed");
//		}else{
//			System.out.println("ok");
//		}
//	}
//}
