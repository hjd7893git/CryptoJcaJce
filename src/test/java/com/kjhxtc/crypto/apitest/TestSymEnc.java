//package com.kjhxtc.crypto.apitest;
//
//import com.kjhxtc.crypto.api.Symmetric.BocSymmetricCrypt;
//import com.kjhxtc.crypto.api.Symmetric.SymEncryptOperator;
//import com.kjhxtc.crypto.util.encoder.Hex;
//
//
///**
// * 标准数据加解密案例
// */
//public class TestSymEnc {
//	static final byte[] key64Bits = Hex.decode("1111222233334444");
//	static final byte[] key128Bits = Hex.decode("11112222333344445555666677778888");
//	static final byte[] key192Bits = Hex.decode("1111222233334444555566667777888899990000AAAABBBB");
//	static final byte[] key256Bits = Hex.decode("1111222233334444555566667777888899990000AAAABBBBCCCCDDDDEEEEFFFF");
//
//	static final BocSymmetricCrypt boclib = new BocSymmetricCrypt();
//
//	static final byte[] iv64Bits = Hex.decode("1234567890ABCDEF");
//	static final byte[] iv128Bits = Hex.decode("1234567890ABCDEFFEDCBA0987654321");
//
//	static byte[] data  = "This is a des 64bits alg! God bless us, @Small".getBytes();
//	/**
//	 * DES使用 64bits密钥
//	 */
//	public static void DesEcb() {
//
//		SymEncryptOperator enc = boclib.BOC_EncryptInit(boclib.ALG_DES_ECB, boclib.PADDING_PKCS7, key64Bits, null);
//		byte [] res = boclib.BOC_Encrypt(enc, data);
//		System.out.println(Hex.toHexString(res));
//	}
//
//	public static void DesCbc() {
//		SymEncryptOperator enc = boclib.BOC_EncryptInit(boclib.ALG_DES_CBC, boclib.PADDING_PKCS7, key64Bits, null);
//		byte [] res = boclib.BOC_Encrypt(enc, data);
//
//		System.out.println(Hex.toHexString(res));
//
//	}
//
//	public static void DESedeECB(){
//		SymEncryptOperator enc;
//		byte [] res;
//
//		enc = boclib.BOC_EncryptInit(boclib.ALG_3DES_ECB, boclib.PADDING_PKCS7, key128Bits, null);
//		res = boclib.BOC_Encrypt(enc, data);
//		System.out.println(Hex.toHexString(data));
//		System.out.println(Hex.toHexString(res));
//		enc = boclib.BOC_EncryptInit(boclib.ALG_3DES_ECB, boclib.PADDING_PKCS7, key192Bits, null);
//		res = boclib.BOC_Encrypt(enc, data);
//
//		System.out.println(Hex.toHexString(res));
//
//	}
//	public static void DESedeCBC(){
//		SymEncryptOperator enc;
//		byte [] res;
//
//		enc = boclib.BOC_EncryptInit(boclib.ALG_3DES_CBC, boclib.PADDING_PKCS7, key128Bits, iv64Bits);
//		res = boclib.BOC_Encrypt(enc, data);
//		System.out.println(Hex.toHexString(data));
//		System.out.println(Hex.toHexString(res));
//
//		enc = boclib.BOC_EncryptInit(boclib.ALG_3DES_CBC, boclib.PADDING_PKCS7, key192Bits, iv64Bits);
//		res = boclib.BOC_Encrypt(enc, data);
//
//		System.out.println(Hex.toHexString(res));
//	}
//	public static void AESECB(){
//		SymEncryptOperator enc;
//		byte [] res;
//
//		enc = boclib.BOC_EncryptInit(boclib.ALG_AES_ECB, boclib.PADDING_PKCS7, key128Bits, null);
//		res = boclib.BOC_Encrypt(enc, data);
//		System.out.println(Hex.toHexString(res));
//
//		enc = boclib.BOC_EncryptInit(boclib.ALG_AES_ECB, boclib.PADDING_PKCS7, key192Bits, null);
//		res = boclib.BOC_Encrypt(enc, data);
//		System.out.println(Hex.toHexString(res));
//
//		enc = boclib.BOC_EncryptInit(boclib.ALG_AES_ECB, boclib.PADDING_PKCS7, key256Bits, null);
//		res = boclib.BOC_Encrypt(enc, data);
//		System.out.println(Hex.toHexString(res));
//	}
//	public static void AESCBC(){
//		SymEncryptOperator enc;
//		byte [] res;
//
//		enc = boclib.BOC_EncryptInit(boclib.ALG_AES_CBC, boclib.PADDING_PKCS7, key128Bits, iv128Bits);
//		res = boclib.BOC_Encrypt(enc, data);
//		System.out.println(Hex.toHexString(res));
//
//		enc = boclib.BOC_EncryptInit(boclib.ALG_AES_CBC, boclib.PADDING_PKCS7, key192Bits, iv128Bits);
//		res = boclib.BOC_Encrypt(enc, data);
//		System.out.println(Hex.toHexString(res));
//
//		enc = boclib.BOC_EncryptInit(boclib.ALG_AES_CBC, boclib.PADDING_PKCS7, key256Bits, iv128Bits);
//		res = boclib.BOC_Encrypt(enc, data);
//		System.out.println(Hex.toHexString(res));
//	}
//	/**
//	 * @param args
//	 */
//	public static void main(String[] args) {
//		System.out.println(Hex.toHexString(data));
////		TestSymEnc.DesEcb();
////		TestSymEnc.DesCbc();
////		TestSymEnc.DESedeECB();
////		TestSymEnc.DESedeCBC();
//		TestSymEnc.AESECB();
//		TestSymEnc.AESCBC();
//
//	}
//}
