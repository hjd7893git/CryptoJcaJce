//package com.kjhxtc.crypto.apitest;
//
//
//import com.kjhxtc.crypto.api.Asymmetric.*;
//import com.kjhxtc.crypto.util.encoder.Hex;
//
//import java.security.NoSuchAlgorithmException;
//import java.security.spec.InvalidKeySpecException;
//import java.security.spec.X509EncodedKeySpec;
//
//
//public class TestAsym {
//    static String plainText = "2342423432423afds.a fd.d,msaf.nmgdadsagdsag";
//    static byte[] m_data = plainText.getBytes();
//
//
//    public static void out(byte[] der) {
//        System.out.println(Hex.toHexString(der));
//    }
//
//    public static void main(String[] args) throws NoSuchAlgorithmException {
//
//        long start = System.currentTimeMillis();
//
//        BocAsymmetricCrypt api = new BocAsymmetricCrypt();
//        AsymmetricKeyPair mkey = api.BOC_GenKeyPair(BocAsymmetricCrypt.KEY_RSA, 1024);
//        byte[] rsa_vk = api.BOC_GetPrivateKey(mkey);
//        byte[] rsa_pk = api.BOC_GetPublicKey(mkey);
////        byte[] rsa_pk = Hex.decode("00000400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000083cd6aee6250b68f936b60773d33d33a3c4980d05d0d061e13c744b97a4b551105dc88c98176bc32a08e9318bdf5eaceeb1de10f24dc4c2c475773895ef7d7f689f935b0cc448b8ad5833c87d38190698e57e7d9a98c67969581694407258fb357c1bed0ee4ed9500d254fbf162a1cd2d6b71e3a53095b3f52fcdd4f5cafec3700010001");
////        byte[] rsa_vk = Hex.decode("00000400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000083cd6aee6250b68f936b60773d33d33a3c4980d05d0d061e13c744b97a4b551105dc88c98176bc32a08e9318bdf5eaceeb1de10f24dc4c2c475773895ef7d7f689f935b0cc448b8ad5833c87d38190698e57e7d9a98c67969581694407258fb357c1bed0ee4ed9500d254fbf162a1cd2d6b71e3a53095b3f52fcdd4f5cafec370001000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007ce8035a7ed9b6e941f014af6bd4dd6ce70d5182256cde4b4abb21aa14f7a75a468fc522120b10377a692385e32056b259a72b4e4ac46b7bdaf04991f9ea2f431cd1d64545d3c319c619f3d35c953f0c2b197cb0ba1db8af9075b48fcd5f1f7baf69854c01a28c0122253ddd4f4523acf59b4d44eafe39b2a38bc1733dfd0b8100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000c0f117a1222399dfbd8b276184ed4e9485f9e77a3a0638d651f8cf2ac655735bec8f50b5dca74a3afe24edbd7ca5868591c37af9b22c2ad6a38baea1eb4ac2b100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000aee0f6ad2b1afcfd2581a10e6e76990b72e2b167ccb7a6586bdf80bc1b9af3a7cca62b35c43818e6de565d58a605928cf03d02e51f46c9c137b3600b8184c76700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000a8fafe8cfb4f5ed6dc34e19ac146ed899b05523c74513aba4a34b58c6b042ff2946eb86b6f037d39c9982237d0d14fcdefe9daec91a5a9272f39890afafe112100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000395e2d2dd3952fe6ef75dddab91c08cabbae681a300b12f3b0b78a28ab39a2b2aa2cdcc1ef93a37fa0f25dfee5bfb03ab1d8a1e97de01ff1558f6f7376d2930300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000a141022ab3ad2825b9f56796465346b21ad70b3594163769e37a3c5fc01c06abb6a566b34dfb6e1e8309c42a736735f63c9913e77c6f79816dd1f2aa4da25c7b");
//        out(rsa_pk);
//        out(rsa_vk);
//
//        try {
//            byte[] apk = KeyUtils.translateToHSMDerofPK(rsa_pk);
//            byte[] pk = KeyUtils.translateFromHSMDerofPK(apk);
//            X509EncodedKeySpec spec = KeyUtils.toX509(rsa_pk);
//            out(spec.getEncoded());
//            byte[] mpk = KeyUtils.getBocPublicKey(spec);
//
//            EncryptOperator en = api.BOC_EncryptInit(api.ID_RSA_PKCS1, pk);
//            byte[] res = api.BOC_EncryptData(en, m_data);
//            out(res);
//            DecryptOperator de = api.BOC_DecryptInit(api.ID_RSA_PKCS1, rsa_vk);
//            byte[] res1 = api.BOC_DecryptData(de, res);
//            System.out.println(Hex.toHexString(res1));
////            EncryptOperator e1 = api.BOC_EncryptInit(api.ID_RSA_PKCS1, mpk);
////            byte[] res1 = api.BOC_EncryptData(en, m_data);
////            DecryptOperator eamNo = api.BOC_DecryptInit(BocAsymmetricCrypt.ID_RSA_NoPad, rsa_vk);
////            DecryptOperator eamNo1 = api.BOC_DecryptInit(BocAsymmetricCrypt.ID_RSA_PKCS1, rsa_vk);
////            byte[] m_eamNo = api.BOC_DecryptData(eamNo, res);
////
////            m_eamNo = api.BOC_DecryptData(eamNo, Hex.decode("509AC03137BC51A7875E1AF4459391BC6A3FEE0CBD5BFD3D308DF6651C4F864EC0E94C8D0C1B780977FAA4125ADF0E2642D4BD0C3A4452FCF04AE029733A65F293CAF93580AB13FB5FB864471A8E8D99A0ED157B8903DC93081D3B6B14AA2435BA98CD8390A932A7CEAD823885940D415561B243439B62EE1EC9FC24874845CB"));
////            System.out.println("解密结果 不去PKCS1填充HEX: " + Hex.toHexString(m_eamNo));
////            m_eamNo = api.BOC_DecryptData(eamNo1, Hex.decode("509AC03137BC51A7875E1AF4459391BC6A3FEE0CBD5BFD3D308DF6651C4F864EC0E94C8D0C1B780977FAA4125ADF0E2642D4BD0C3A4452FCF04AE029733A65F293CAF93580AB13FB5FB864471A8E8D99A0ED157B8903DC93081D3B6B14AA2435BA98CD8390A932A7CEAD823885940D415561B243439B62EE1EC9FC24874845CB"));
////            System.out.println("解密结果 去PKCS1填充HEX: " + Hex.toHexString(m_eamNo));
//
//        } catch (InvalidKeySpecException e) {
//            e.printStackTrace();
//        }
////		System.out.println(Hex.toHexString(rsa_vk));
////		System.out.println(Hex.toHexString(rsa_pk));
//
////
////		System.out.println("原始明文      : " + plainText);
////		System.out.println("原始明文 HEX: " + Hex.toHexString(m_data));
////		EncryptOperator mea = api.BOC_EncryptInit(BocAsymmetricCrypt.ID_SM2_NoPad, rsa_pk);
////		byte[] m_res = api.BOC_EncryptData(mea, m_data);
////		System.out.println("加密结果: " + Hex.toHexString(m_res));
////		DecryptOperator eamNo = api.BOC_DecryptInit(BocAsymmetricCrypt.ID_SM2_NoPad, rsa_vk);
////		byte[] m_eamNo = api.BOC_DecryptData(eamNo, m_res);
////		System.out.println("解密结果 不去PKCS1填充HEX: " + Hex.toHexString(m_eamNo));
////		DecryptOperator eam = api.BOC_DecryptInit(BocAsymmetricCrypt.ID_SM2_NoPad, rsa_vk);
////		byte[] m_data1 = api.BOC_DecryptData(eam, m_res);
////		System.out.println("解密结果   去PKCS1填充HEX: " + Hex.toHexString(m_data1));
////		System.out.println("解密结果 原始明文"+new String(m_data1));
////
////
////		byte [] sin_data = "234adsgd\23asgaserfaesr".getBytes();
////		SignOpreator sin= api.BOC_SignInit(BocAsymmetricCrypt.SIG_RSA_MD5, rsa_vk);
////		byte[] sin_res = api.BOC_SignData(sin, sin_data);
////		VerifyOpreator ver = api.BOC_VerifyInit(BocAsymmetricCrypt.SIG_RSA_MD5, rsa_pk);
////		int ver_code = api.BOC_VerifySign(ver, rsa_pk, sin_data, sin_res);
////		System.out.println(ver_code);
////		byte[] pk = Hex.decode("00000100"+
////				"0000000000000000000000000000000000000000000000000000000000000000" +
////				"EE75C744F2BDBF244A01D352500B141ED0696E3F1189EFBA36D3FD664DF2DD7C" +
////				"0000000000000000000000000000000000000000000000000000000000000000" +
////				"2DA19F42BB394B5F3BFB11D06432E22B37D6BA3CB1790A2C01527A85C1DA5DEC" );
//
////		byte[] pk = Hex.decode("00000100"+
////				"0000000000000000000000000000000000000000000000000000000000000000" +
////				"435B39CCA8F3B508C1488AFC67BE491A0F7BA07E581A0E4849A5CF70628A7E0A" +
////				"0000000000000000000000000000000000000000000000000000000000000000" +
////				"75DDBA78F15FEECB4C7895E2C1CDF5FE01DEBB2CDBADF45399CCF77BBA076A42" );
////
////		byte[] mdata= "encryption standard".getBytes();
////		EncryptOperator enc= api.BOC_EncryptInit(BocAsymmetricCrypt.ID_SM2_NoPad, pk);
////		byte[] enc_res;
////		enc_res = api.BOC_EncryptData(enc, mdata);
////
////		System.out.println(Hex.toHexString(enc_res) );
////		byte[] vk = Hex.decode("00000100"+
////				"0000000000000000000000000000000000000000000000000000000000000000" +
////				"435B39CCA8F3B508C1488AFC67BE491A0F7BA07E581A0E4849A5CF70628A7E0A" +
////				"0000000000000000000000000000000000000000000000000000000000000000" +
////				"75DDBA78F15FEECB4C7895E2C1CDF5FE01DEBB2CDBADF45399CCF77BBA076A42" +
////				"0000000000000000000000000000000000000000000000000000000000000000" +
////				"1649AB77A00637BD5E2EFE283FBF353534AA7F7CB89463F208DDBC2920BB0DA0"
////		);
////		DecryptOperator dec = api.BOC_DecryptInit(BocAsymmetricCrypt.ID_SM2_NoPad, vk);
////		byte[] dec_res;
////		try {
////			dec_res =dec.processBytes(enc_res);
////			System.out.println(Hex.toHexString(dec_res) );
////		} catch (InvalidCipherTextException e) {
////			e.printStackTrace();
////		}
////		byte[] pk = Hex.decode("00000100"+
////				"0000000000000000000000000000000000000000000000000000000000000000" +
////				"0AE4C7798AA0F119471BEE11825BE46202BB79E2A5844495E97C04FF4DF2548A" +
////				"0000000000000000000000000000000000000000000000000000000000000000" +
////				"7C0240F88F1CD4E16352A73C17B7F16F07353E53A176D684A9FE0C6BB798E857"
////				);
////		byte[] vk = Hex.decode("00000100"+
////				"0000000000000000000000000000000000000000000000000000000000000000" +
////				"0AE4C7798AA0F119471BEE11825BE46202BB79E2A5844495E97C04FF4DF2548A" +
////				"0000000000000000000000000000000000000000000000000000000000000000" +
////				"7C0240F88F1CD4E16352A73C17B7F16F07353E53A176D684A9FE0C6BB798E857" +
////				"0000000000000000000000000000000000000000000000000000000000000000" +
////				"128B2FA8BD433C6C068C8D803DFF79792A519A55171B1B650C23661D15897263"
////		);
//
////		AsymmetricKeyPair a=api.BOC_GenKeyPair(BocAsymmetricCrypt.KEY_SM2, 256);
////		byte[] sm2_vk = api.BOC_GetPrivateKey(a);
////		byte[] sm2_pk = api.BOC_GetPublicKey(a);
////		byte[] msg= "encryption standard".getBytes();
////		System.out.println("VK::"+Hex.toHexString(sm2_vk) );
////		EncryptOperator sm2enc= api.BOC_EncryptInit(BocAsymmetricCrypt.ID_SM2_NoPad, sm2_pk);
////		byte[] sm2ecn_res = api.BOC_EncryptData(sm2enc, msg);
////		DecryptOperator sm2dec = api.BOC_DecryptInit(BocAsymmetricCrypt.ID_SM2_NoPad, sm2_vk);
////		byte[] sm2dec_res = api.BOC_DecryptData(sm2dec, sm2ecn_res);
////		System.out.println(new String(sm2dec_res));
////
////		SignOpreator sig=api.BOC_SignInit(BocAsymmetricCrypt.SIG_SM2_SM3, sm2_vk);
////
////		byte[] sig_res = api.BOC_SignData(sig, msg);
////		System.out.println("R+S:"+Hex.toHexString(sig_res) );
////
////		VerifyOpreator vry = api.BOC_VerifyInit(BocAsymmetricCrypt.SIG_SM2_SM3, sm2_pk);
////		int vry_res = api.BOC_VerifySign(vry, sm2_pk, msg, sig_res);
////
////		System.out.println(vry_res);
////
////		long end = System.currentTimeMillis();
////		System.out.println("OK:total " +( end-start) +"ms");
////		AsymmetricCrypt api = new AsymmetricCrypt();
////		AsymmetricKeyPair a = api.BOC_GenKeyPair(api.RSA_KEY, 1024);
////		String res = Hex.toHexString(api.BOC_GetPrivateKey(a));
////		System.out.println(res);
//    }
//
//}
