package com.kedyy;

import com.kjhxtc.crypto.digests.SM3Digest;
import com.kjhxtc.crypto.engines.SM2Core;
import com.kjhxtc.jcajce.provider.asymmetric.sm2.SM2p256v1PrivateCrtKey;
import com.kjhxtc.jcajce.provider.asymmetric.sm2.SM2p256v1PublicKey;
import com.kjhxtc.math.ec.ECPoint;
import com.kjhxtc.util.encoders.Hex;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;

public class TestKageee {

    public static void main(String[] args) throws Exception {
        byte[] IDa = "ALICE123@YAHOO.COM".getBytes();
        byte[] IDb = "BILL456@YAHOO.COM".getBytes();
        SM2p256v1PrivateCrtKey LocalPK = new SM2p256v1PrivateCrtKey(
                new BigInteger("3099093BF3C137D8FCBBCDF4A2AE50F3B0F216C3122D79425FE03A45DBFE1655", 16),
                new BigInteger("3DF79E8DAC1CF0ECBAA2F2B49D51A4B387F2EFAF482339086A27A8E05BAED98B", 16),
                new BigInteger("6FCBA2EF9AE0AB902BC3BDE3FF915D44BA4CC78F88E2F8E7F8996D3B8CCEEDEE", 16)
        );


        SM2p256v1PrivateCrtKey TempKey = new SM2p256v1PrivateCrtKey(
                new BigInteger("6CB5633816F4DD560B1DEC458310CBCC6856C09505324A6D23150C408F162BF0", 16),
                new BigInteger("0D6FCF62F1036C0A1B6DACCF57399223A65F7D7BF2D9637E5BBBEB857961BF1A", 16),
                new BigInteger("83A2C9C8B96E5AF70BD480B472409A9A327257F1EBB73F5B073354B248668563", 16));

        SM2p256v1PublicKey PeerRB = new SM2p256v1PublicKey(
                new BigInteger("1799B2A2C778295300D9A2325C686129B8F2B5337B3DCF4514E8BBC19D900EE5", 16),
                new BigInteger("54C9288C82733EFDF7808AE7F27D0E732F7C73A7D9AC98B7D8740A91D0DB3CF4", 16));

        SM2p256v1PublicKey PeerXB = new SM2p256v1PublicKey(
                new BigInteger("245493D446C38D8CC0F118374690E7DF633A8A4BFB3329B5ECE604B2B4F37F43", 16),
                new BigInteger("53C0869F4B9E17773DE68FEC45E14904E0DEA45BF6CECF9918C85EA047C60A4C", 16));


        byte[] Za = SM2Core.getSignerA_Z(IDa, LocalPK.getQ(), new SM3Digest());
        byte[] Zb = SM2Core.getSignerA_Z(IDb, PeerXB.getQ(), new SM3Digest());
        System.out.println("Za::" + Hex.toHexString(Za));

        ECPoint tmpKey = TempKey.getQ();
        BigInteger w = new BigInteger("127");
        BigInteger xLocal = BigInteger.valueOf(2L).pow(w.intValue()).add(
                TempKey.getQ().getXCoord().toBigInteger().and(BigInteger.valueOf(2L).pow(w.intValue()).subtract(BigInteger.ONE))
        );
        System.out.println("-X1::" + xLocal.toString(16));
        BigInteger tLocal = LocalPK.getD().add(xLocal.multiply(TempKey.getD())).mod(SM2Core.getN());
        System.out.println("-Ta::" + tLocal.toString(16));

        BigInteger xPeer = BigInteger.valueOf(2L).pow(w.intValue()).add(
                PeerRB.getQ().getXCoord().toBigInteger().and(BigInteger.valueOf(2L).pow(w.intValue()).subtract(BigInteger.ONE))
        );
        System.out.println("-X2::" + xPeer.toString(16));

        //ECPoint point = PeerRB.getQ().add(tmpKey.multiply(xPeer)).multiply(tLocal.multiply(SM2Core.getH()));
        ECPoint point = PeerRB.getQ().multiply(xPeer).normalize();
        System.out.println("XB0::" + point.getXCoord().toBigInteger().toString(16));
        System.out.println("YB0::" + point.getYCoord().toBigInteger().toString(16));

        ECPoint point1= PeerXB.getQ().add(point).normalize();
        System.out.println("XB1::" + point1.getXCoord().toBigInteger().toString(16));
        System.out.println("YB1::" + point1.getYCoord().toBigInteger().toString(16));
        ECPoint u = point1.multiply(tLocal.multiply(SM2Core.getH())).normalize();
        System.out.println("XU::" + u.getXCoord().toBigInteger().toString(16));
        System.out.println("YU::" + u.getYCoord().toBigInteger().toString(16));
        ByteArrayOutputStream bf= new ByteArrayOutputStream();
        bf.write(u.getEncoded(),1,64);
        bf.write(Za);
        bf.write(Zb);
        byte[] key =SM2Core.kdf(bf.toByteArray(), 16, new SM3Digest());
        System.out.println(Hex.toHexString(key));
    }
}
