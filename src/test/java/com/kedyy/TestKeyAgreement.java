package com.kedyy;

import com.kjhxtc.crypto.params.SM2PrivateKeyParameters;
import com.kjhxtc.crypto.params.SM2PublicKeyParameters;
import com.kjhxtc.crypto.signers.SM2DigestSigner;
import com.kjhxtc.jcajce.provider.asymmetric.sm2.SM2p256v1PrivateCrtKey;
import com.kjhxtc.jcajce.provider.asymmetric.sm2.SM2p256v1PublicKey;
import com.kjhxtc.jcajce.spec.SM2kaParameterSpec;
import com.kjhxtc.util.encoders.Hex;

import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.SecretKey;
import java.math.BigInteger;
import java.security.*;

public class TestKeyAgreement {
    static {
        Security.addProvider(new com.kjhxtc.jce.provider.KJHXTCProvider());
    }

    public static void main(String[] args) throws Exception {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("SM2", "KJHXTC");
        keyPairGenerator.initialize(256);

        KeyPair sm2Kp = keyPairGenerator.generateKeyPair();
        KeyAgreement kagA = KeyAgreement.getInstance("SM2withSM3KDF", "KJHXTC");
        kagA.init(sm2Kp.getPrivate(), new SM2kaParameterSpec(false, "alice@126.com".getBytes(), "bob@163.com".getBytes()));

        //KeyFactory keyFactory = KeyFactory.getInstance("SM2", "KJHXTC");
        //keyFactory.generatePrivate(new PKCS8EncodedKeySpec(sm2Kp.getPrivate().getEncoded()));

        KeyPair sm2Kp2 = keyPairGenerator.generateKeyPair();
        KeyAgreement kagB = KeyAgreement.getInstance("SM2withSM3KDF", "KJHXTC");
        kagB.init(sm2Kp2.getPrivate(), new SM2kaParameterSpec(true, "bob@163.com".getBytes(), "alice@126.com".getBytes()));

        PublicKey ra = (PublicKey) kagA.doPhase(sm2Kp2.getPublic(), false);
        PublicKey rb = (PublicKey) kagB.doPhase(sm2Kp.getPublic(), false);

        kagA.doPhase(rb, true);
        kagB.doPhase(ra, true);
        SecretKey skA = kagA.generateSecret("SM4");
        System.out.println(Hex.toHexString(skA.getEncoded()));
        SecretKey skB = kagB.generateSecret("SM4");
        System.out.println(Hex.toHexString(skB.getEncoded()));

        Cipher enc = Cipher.getInstance("SM2", "KJHXTC");
        enc.init(Cipher.ENCRYPT_MODE, sm2Kp.getPublic());
        System.out.println(sm2Kp.getPrivate());
        byte[] c = enc.doFinal("Hello World!".getBytes());

        System.out.println(Hex.toHexString("Hello World!".getBytes()));
        System.out.println(Hex.toHexString(c));

        Cipher dec = Cipher.getInstance("SM2", "KJHXTC");
        dec.init(Cipher.DECRYPT_MODE, sm2Kp.getPrivate());
        byte[] s = dec.doFinal(c);
        System.out.println(new String(s));


        SM2p256v1PrivateCrtKey signKey = new SM2p256v1PrivateCrtKey(
                new BigInteger("28515D31EC34E05087EEDC6141F611F7214C8B93A14C7421E412047CD1EFDBC6", 16),
                new BigInteger("449357AB8DD27CB9E68C83C2354F1474C0E5F0B347FC4ED3EEC63065B12E7F8D", 16),
                new BigInteger("190e1100f9e0f6211af5c3cd1ca9617183875fd962b198968fc200116c234a17", 16));
        Signature signature = Signature.getInstance("SM3withSM2", "KJHXTC");
        signature.initSign(signKey);
        signature.update("Hello World".getBytes());
        System.out.println(Hex.toHexString(signature.sign()));

        Signature verify = Signature.getInstance("SM3withSM2", "KJHXTC");
        verify.initVerify(new SM2p256v1PublicKey(signKey.getQ().getEncoded()));
        boolean pass = verify.verify(Hex.decode("78d6a1b3aafb2241a2ce72b7d74da4ff14691f08f352504cf4260f50b4472a7be553f2ad60c8596fe1dc284e967100305f5066fe23459c0b42d5df93e2827e33"));
        //if(!pass)throw new IllegalStateException("Verify Failed");
        SM2DigestSigner sm2DigestSigner = new SM2DigestSigner(null, null);
        sm2DigestSigner.init(true, new SM2PrivateKeyParameters(signKey.getD()));
        sm2DigestSigner.update("Hello World".getBytes(), 0, "Hello World".getBytes().length);
        System.out.println(Hex.toHexString(sm2DigestSigner.generateSignature()));
        SM2DigestSigner sm2Verfiy = new SM2DigestSigner(null, null);
        sm2Verfiy.init(false, new SM2PublicKeyParameters(signKey.getQ()));
        sm2Verfiy.update("Hello World".getBytes(), 0, "Hello World".getBytes().length);
        boolean stat = sm2Verfiy.verifySignature(Hex.decode("EE05C606D84126E63AEF101E44F6A584DAD926AFB098B34009C669F99582C71FE2073E9C3707A1EECF841079C281B47CBC4709FCE9A9C8F349EB2707FDAFE06A"));
        System.out.println(stat);
    }
}
