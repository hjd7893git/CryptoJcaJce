package com.kedyy;

import com.kjhxtc.crypto.util.encoder.Hex;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayOutputStream;
import java.security.Security;

public class Sec {
    public static void main(String[] args) throws Exception {
        Security.addProvider(new com.kjhxtc.jce.provider.KJHXTCProvider());
//        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("SM2", "KJHXTC");
//        keyPairGenerator.initialize(256);
//        KeyPair sm2Kp = keyPairGenerator.generateKeyPair();
//        System.out.println(sm2Kp.getPrivate());
//        System.out.println(sm2Kp.getPublic());
//
//        MessageDigest messageDigest = MessageDigest.getInstance("SM3", "KJHXTC");
//        messageDigest.update("Hello World!".getBytes());
//        System.out.println(Hex.toHexString(messageDigest.digest()));
        ByteArrayOutputStream of =new ByteArrayOutputStream();

        for (int i = 0; i < 256; i++) {
            of.write(i);
        }
        System.out.println(Hex.toHexString(of.toByteArray()));

        Cipher cipher = Cipher.getInstance("AES/CFB1/NoPadding", "KJHXTC");
        KeyGenerator keyGenerator = KeyGenerator.getInstance("SM4", "KJHXTC");
//        SecretKey sm4Key = keyGenerator.generateKey();
        SecretKey sm4Key = new SecretKeySpec(Hex.decode("2B7E151628AED2A6ABF7158809CF4F3C"), "AES");
        System.out.println("KEY ->" + Hex.toHexString(sm4Key.getEncoded()));
        cipher.init(Cipher.ENCRYPT_MODE, sm4Key, new IvParameterSpec(Hex.decode("000102030405060708090A0B0C0D0E0F")));
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] text = Hex.decode("6BC1BEE22E409F96E93D7E117393172A52");
        outputStream.write(cipher.update(text));
        outputStream.write(cipher.doFinal());
        System.out.println("TEXT->" + Hex.toHexString(text));
        System.out.println("DATA->" + Hex.toHexString(outputStream.toByteArray()));
        cipher.init(Cipher.DECRYPT_MODE, sm4Key, new IvParameterSpec(Hex.decode("000102030405060708090A0B0C0D0E0F")));
        byte[] e=cipher.update(outputStream.toByteArray());
        System.out.println("DEXT->" + Hex.toHexString(e));

    }
}
