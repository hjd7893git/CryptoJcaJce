package com.kjhxtc.jcajce.provider.util;

import com.kjhxtc.jcajce.provider.config.ConfigurableProvider;

public abstract class AlgorithmProvider
{
    public abstract void configure(ConfigurableProvider provider);
}
