package com.kjhxtc.jcajce.provider.symmetric;

import com.kjhxtc.crypto.CipherKeyGenerator;
import com.kjhxtc.crypto.engines.XSalsa20Engine;
import com.kjhxtc.jcajce.provider.config.ConfigurableProvider;
import com.kjhxtc.jcajce.provider.symmetric.util.BaseKeyGenerator;
import com.kjhxtc.jcajce.provider.symmetric.util.BaseStreamCipher;
import com.kjhxtc.jcajce.provider.util.AlgorithmProvider;

public final class XSalsa20
{
    private XSalsa20()
    {
    }
    
    public static class Base
        extends BaseStreamCipher
    {
        public Base()
        {
            super(new XSalsa20Engine(), 24);
        }
    }

    public static class KeyGen
        extends BaseKeyGenerator
    {
        public KeyGen()
        {
            super("XSalsa20", 256, new CipherKeyGenerator());
        }
    }

    public static class Mappings
        extends AlgorithmProvider
    {
        private static final String PREFIX = XSalsa20.class.getName();

        public Mappings()
        {
        }

        public void configure(ConfigurableProvider provider)
        {

            provider.addAlgorithm("Cipher.XSALSA20", PREFIX + "$Base");
            provider.addAlgorithm("KeyGenerator.XSALSA20", PREFIX + "$KeyGen");

        }
    }
}
