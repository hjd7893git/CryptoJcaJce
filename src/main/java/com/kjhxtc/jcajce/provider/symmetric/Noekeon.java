package com.kjhxtc.jcajce.provider.symmetric;

import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.spec.IvParameterSpec;

import com.kjhxtc.crypto.BlockCipher;
import com.kjhxtc.crypto.CipherKeyGenerator;
import com.kjhxtc.crypto.engines.NoekeonEngine;
import com.kjhxtc.crypto.generators.Poly1305KeyGenerator;
import com.kjhxtc.crypto.macs.GMac;
import com.kjhxtc.crypto.modes.GCMBlockCipher;
import com.kjhxtc.jcajce.provider.config.ConfigurableProvider;
import com.kjhxtc.jcajce.provider.symmetric.util.BaseAlgorithmParameterGenerator;
import com.kjhxtc.jcajce.provider.symmetric.util.BaseBlockCipher;
import com.kjhxtc.jcajce.provider.symmetric.util.BaseKeyGenerator;
import com.kjhxtc.jcajce.provider.symmetric.util.BaseMac;
import com.kjhxtc.jcajce.provider.symmetric.util.BlockCipherProvider;
import com.kjhxtc.jcajce.provider.symmetric.util.IvAlgorithmParameters;
import com.kjhxtc.jce.provider.KJHXTCProvider;

public final class Noekeon
{
    private Noekeon()
    {
    }

    public static class ECB
        extends BaseBlockCipher
    {
        public ECB()
        {
            super(new BlockCipherProvider()
            {
                public BlockCipher get()
                {
                    return new NoekeonEngine();
                }
            });
        }
    }

    public static class KeyGen
        extends BaseKeyGenerator
    {
        public KeyGen()
        {
            super("Noekeon", 128, new CipherKeyGenerator());
        }
    }

    public static class GMAC
        extends BaseMac
    {
        public GMAC()
        {
            super(new GMac(new GCMBlockCipher(new NoekeonEngine())));
        }
    }

    public static class Poly1305
        extends BaseMac
    {
        public Poly1305()
        {
            super(new com.kjhxtc.crypto.macs.Poly1305(new NoekeonEngine()));
        }
    }

    public static class Poly1305KeyGen
        extends BaseKeyGenerator
    {
        public Poly1305KeyGen()
        {
            super("Poly1305-Noekeon", 256, new Poly1305KeyGenerator());
        }
    }

    public static class AlgParamGen
        extends BaseAlgorithmParameterGenerator
    {
        protected void engineInit(
            AlgorithmParameterSpec genParamSpec,
            SecureRandom random)
            throws InvalidAlgorithmParameterException
        {
            throw new InvalidAlgorithmParameterException("No supported AlgorithmParameterSpec for Noekeon parameter generation.");
        }

        protected AlgorithmParameters engineGenerateParameters()
        {
            byte[] iv = new byte[16];

            if (random == null)
            {
                random = new SecureRandom();
            }

            random.nextBytes(iv);

            AlgorithmParameters params;

            try
            {
                params = AlgorithmParameters.getInstance("Noekeon", KJHXTCProvider.PROVIDER_NAME);
                params.init(new IvParameterSpec(iv));
            }
            catch (Exception e)
            {
                throw new RuntimeException(e.getMessage());
            }

            return params;
        }
    }

    public static class AlgParams
        extends IvAlgorithmParameters
    {
        protected String engineToString()
        {
            return "Noekeon IV";
        }
    }

    public static class Mappings
        extends SymmetricAlgorithmProvider
    {
        private static final String PREFIX = Noekeon.class.getName();

        public Mappings()
        {
        }

        public void configure(ConfigurableProvider provider)
        {

            provider.addAlgorithm("AlgorithmParameters.NOEKEON", PREFIX + "$AlgParams");

            provider.addAlgorithm("AlgorithmParameterGenerator.NOEKEON", PREFIX + "$AlgParamGen");

            provider.addAlgorithm("Cipher.NOEKEON", PREFIX + "$ECB");

            provider.addAlgorithm("KeyGenerator.NOEKEON", PREFIX + "$KeyGen");

            addGMacAlgorithm(provider, "NOEKEON", PREFIX + "$GMAC", PREFIX + "$KeyGen");
            addPoly1305Algorithm(provider, "NOEKEON", PREFIX + "$Poly1305", PREFIX + "$Poly1305KeyGen");
        }
    }
}
