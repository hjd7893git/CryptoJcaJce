package com.kjhxtc.jcajce.provider.symmetric.util;

import com.kjhxtc.crypto.BlockCipher;

public interface BlockCipherProvider
{
    BlockCipher get();
}
