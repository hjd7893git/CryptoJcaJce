package com.kjhxtc.jcajce.provider.symmetric;

import com.kjhxtc.asn1.cngmt.GMTObjectIdentifiers;
import com.kjhxtc.crypto.BufferedBlockCipher;
import com.kjhxtc.crypto.CipherKeyGenerator;
import com.kjhxtc.crypto.KeyGenerationParameters;
import com.kjhxtc.crypto.engines.SMS4Engine;
import com.kjhxtc.crypto.macs.CBCBlockCipherMac;
import com.kjhxtc.crypto.macs.CMac;
import com.kjhxtc.crypto.macs.GMac;
import com.kjhxtc.crypto.modes.CBCBlockCipher;
import com.kjhxtc.crypto.modes.CFBBlockCipher;
import com.kjhxtc.crypto.modes.GCMBlockCipher;
import com.kjhxtc.crypto.modes.OFBBlockCipher;
import com.kjhxtc.jcajce.provider.config.ConfigurableProvider;
import com.kjhxtc.jcajce.provider.symmetric.util.*;
import com.kjhxtc.jcajce.provider.util.AlgorithmProvider;
import com.kjhxtc.jce.provider.KJHXTCProvider;

import javax.crypto.spec.IvParameterSpec;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidParameterException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;

public final class SMS4
{
    private SMS4()
    {
    }

    public static class Base
            extends BaseBlockCipher
    {
        public Base()
        {
            super(new SMS4Engine());
        }
    }

    public static class CBC
            extends BaseBlockCipher
    {
        public CBC()
        {
            super(new CBCBlockCipher(new SMS4Engine()), 128);
        }
    }

    static public class CFB
            extends BaseBlockCipher
    {
        public CFB()
        {
            super(new BufferedBlockCipher(new CFBBlockCipher(new SMS4Engine(), 128)), 128);
        }
    }

    static public class OFB
            extends BaseBlockCipher
    {
        public OFB()
        {
            super(new BufferedBlockCipher(new OFBBlockCipher(new SMS4Engine(), 128)), 128);
        }
    }

    static public class GCM
            extends BaseBlockCipher
    {
        public GCM()
        {
            super(new GCMBlockCipher(new SMS4Engine()));
        }
    }

    public static class SM4CMAC
            extends BaseMac
    {
        public SM4CMAC()
        {
            super(new CMac(new SMS4Engine()));
        }
    }

    public static class SM4CBCMAC
            extends BaseMac
    {
        public SM4CBCMAC()
        {
            super(new CBCBlockCipherMac(new SMS4Engine(), 128));
        }
    }

    public static class SM4GMAC
            extends BaseMac
    {
        public SM4GMAC()
        {
            super(new GMac(new GCMBlockCipher(new SMS4Engine())));
        }
    }

    public static class AlgParamGen
            extends BaseAlgorithmParameterGenerator
    {
        protected void engineInit(
                AlgorithmParameterSpec genParamSpec,
                SecureRandom random)
                throws InvalidAlgorithmParameterException
        {
            throw new InvalidAlgorithmParameterException("No supported AlgorithmParameterSpec for SM4 parameter generation.");
        }

        protected AlgorithmParameters engineGenerateParameters()
        {
            byte[] iv = new byte[16];

            if (random == null)
            {
                random = new SecureRandom();
            }

            random.nextBytes(iv);

            AlgorithmParameters params;

            try
            {
                params = AlgorithmParameters.getInstance("SM4", KJHXTCProvider.PROVIDER_NAME);
                params.init(new IvParameterSpec(iv));
            } catch (Exception e)
            {
                throw new RuntimeException(e.getMessage());
            }

            return params;
        }
    }

    public static class AlgParams
            extends IvAlgorithmParameters
    {
        protected String engineToString()
        {
            return "SM4 IV";
        }
    }

    public static class SM4KeyGen
            extends BaseKeyGenerator
    {
        public SM4KeyGen()
        {
            super("SM4", 128, new CipherKeyGenerator());
        }

        protected void engineInit(
                int keySize,
                SecureRandom random)
        {
            try
            {
                if (128 != keySize)
                {
                    throw new IllegalArgumentException("SM4 KeySize Only Support 128 bits");
                }
                if (random == null)
                {
                    random = new SecureRandom();
                }
                engine.init(new KeyGenerationParameters(random, keySize));
                uninitialised = false;
            } catch (IllegalArgumentException e)
            {
                throw new InvalidParameterException(e.getMessage());
            }
        }
    }

    public static class Mappings
            extends AlgorithmProvider
    {
        private static final String PREFIX = SMS4.class.getName();

        public Mappings()
        {
        }

        public void configure(ConfigurableProvider provider)
        {

            provider.addAlgorithm("AlgorithmParameters.SM4", PREFIX + "$AlgParams");
            provider.addAlgorithm("Alg.Alias.AlgorithmParameters." + GMTObjectIdentifiers.id_sm4, "SM4");
            provider.addAlgorithm("Alg.Alias.AlgorithmParameters." + GMTObjectIdentifiers.id_sm4_ecb, "SM4");
            provider.addAlgorithm("Alg.Alias.AlgorithmParameters." + GMTObjectIdentifiers.id_sm4_cbc, "SM4");
            provider.addAlgorithm("Alg.Alias.AlgorithmParameters." + GMTObjectIdentifiers.id_sm4_ofb, "SM4");
            provider.addAlgorithm("Alg.Alias.AlgorithmParameters." + GMTObjectIdentifiers.id_sm4_cfb, "SM4");


            provider.addAlgorithm("AlgorithmParameterGenerator.SM4", PREFIX + "$AlgParamGen");
            provider.addAlgorithm("Alg.Alias.AlgorithmParameterGenerator." + GMTObjectIdentifiers.id_sm4, "SM4");
            provider.addAlgorithm("Alg.Alias.AlgorithmParameterGenerator." + GMTObjectIdentifiers.id_sm4_ecb, "SM4");
            provider.addAlgorithm("Alg.Alias.AlgorithmParameterGenerator." + GMTObjectIdentifiers.id_sm4_cbc, "SM4");
            provider.addAlgorithm("Alg.Alias.AlgorithmParameterGenerator." + GMTObjectIdentifiers.id_sm4_cfb, "SM4");
            provider.addAlgorithm("Alg.Alias.AlgorithmParameterGenerator." + GMTObjectIdentifiers.id_sm4_ofb, "SM4");


            provider.addAlgorithm("Cipher.SM4", PREFIX + "$Base");
            provider.addAlgorithm("Alg.Alias.Cipher." + GMTObjectIdentifiers.id_sm4, "SM4");
            provider.addAlgorithm("Alg.Alias.Cipher." + GMTObjectIdentifiers.id_sm4_ecb, "SM4");
            provider.addAlgorithm("Cipher." + GMTObjectIdentifiers.id_sm4_cbc, PREFIX + "$CBC");
            provider.addAlgorithm("Cipher." + GMTObjectIdentifiers.id_sm4_ofb, PREFIX + "$OFB");
            provider.addAlgorithm("Cipher." + GMTObjectIdentifiers.id_sm4_cfb, PREFIX + "$CFB");

            provider.addAlgorithm("KeyGenerator.SM4", PREFIX + "$SM4KeyGen");
            provider.addAlgorithm("Alg.Alias.KeyGenerator." + GMTObjectIdentifiers.id_sm4, "SM4");
            provider.addAlgorithm("Alg.Alias.KeyGenerator." + GMTObjectIdentifiers.id_sm4_ecb, "SM4");
            provider.addAlgorithm("Alg.Alias.KeyGenerator." + GMTObjectIdentifiers.id_sm4_cbc, "SM4");
            provider.addAlgorithm("Alg.Alias.KeyGenerator." + GMTObjectIdentifiers.id_sm4_ofb, "SM4");
            provider.addAlgorithm("Alg.Alias.KeyGenerator." + GMTObjectIdentifiers.id_sm4_cfb, "SM4");


            provider.addAlgorithm("Mac.SM4MAC", PREFIX + "$SM4CBCMAC");
            provider.addAlgorithm("Mac.SM4CMAC", PREFIX + "$SM4CMAC");
            provider.addAlgorithm("Alg.Alias.Mac.SM4", "SM4MAC");
            provider.addAlgorithm("Alg.Alias.Mac." + GMTObjectIdentifiers.id_sm4, "SM4MAC");
        }
    }
}
