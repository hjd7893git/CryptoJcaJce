package com.kjhxtc.jcajce.provider.digest;

import com.kjhxtc.crypto.CipherKeyGenerator;
import com.kjhxtc.crypto.digests.SM3Digest;
import com.kjhxtc.crypto.macs.HMac;
import com.kjhxtc.jcajce.provider.config.ConfigurableProvider;
import com.kjhxtc.jcajce.provider.symmetric.util.BaseKeyGenerator;
import com.kjhxtc.jcajce.provider.symmetric.util.BaseMac;

public class SM3 {
    private SM3() {
    }

    static public class Digest
            extends BCMessageDigest
            implements Cloneable {
        public Digest() {
            super(new SM3Digest());
        }

        public Object clone()
                throws CloneNotSupportedException {
            Digest d = (Digest) super.clone();
            d.digest = new SM3Digest((SM3Digest) digest);

            return d;
        }
    }

    /**
     * SM3 HMac
     */
    public static class HashMac
            extends BaseMac {
        public HashMac() {
            super(new HMac(new SM3Digest()));
        }
    }

    public static class KeyGenerator
            extends BaseKeyGenerator {
        public KeyGenerator() {
            super("HMACSM3", 256, new CipherKeyGenerator());
        }
    }

    /**
     * SM3 HMac
     */
    public static class SM3Mac
            extends BaseMac {
        public SM3Mac() {
            super(new HMac(new SM3Digest()));
        }
    }

    public static class Mappings
            extends DigestAlgorithmProvider {
        private static final String PREFIX = SM3.class.getName();

        public Mappings() {
        }

        public void configure(ConfigurableProvider provider) {
            addHMACAlgorithm(provider, "SM3", PREFIX + "$HashMac", PREFIX + "$KeyGenerator");

            provider.addAlgorithm("MessageDigest.SM3", PREFIX + "$Digest");
            provider.addAlgorithm("Alg.Alias.MessageDigest.SM3", "SM3");
            provider.addAlgorithm("Alg.Alias.MessageDigest.1.2.156.197.1.401", "SM3");
        }
    }
}
