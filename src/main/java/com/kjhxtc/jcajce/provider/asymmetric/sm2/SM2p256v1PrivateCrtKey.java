package com.kjhxtc.jcajce.provider.asymmetric.sm2;

import com.kjhxtc.asn1.*;
import com.kjhxtc.asn1.cngmt.GMTObjectIdentifiers;
import com.kjhxtc.asn1.x509.AlgorithmIdentifier;
import com.kjhxtc.crypto.params.AsymmetricKeyParameter;
import com.kjhxtc.crypto.params.SM2PrivateKeyParameters;
import com.kjhxtc.crypto.params.SM2PublicKeyParameters;
import com.kjhxtc.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import com.kjhxtc.jce.ECNamedCurveTable;
import com.kjhxtc.jce.provider.KJHXTCProvider;
import com.kjhxtc.jce.spec.ECPrivateKeySpec;
import com.kjhxtc.math.ec.ECPoint;
import com.kjhxtc.util.BigIntegers;

import java.math.BigInteger;
import java.util.Enumeration;

/**
 * @deprecated use {@link com.kjhxtc.jcajce.provider.asymmetric.ec.BCECPrivateKey} instead
 */
@Deprecated
public class SM2p256v1PrivateCrtKey extends SM2p256v1PrivateKey {

    private static final long serialVersionUID = 3875983437142380052L;

    public SM2p256v1PrivateCrtKey(AsymmetricKeyParameter vk, AsymmetricKeyParameter pk) {
        super(vk);
        if (pk.isPrivate() || !(pk instanceof SM2PublicKeyParameters))
            throw new IllegalArgumentException("SM2 Public key required");
        this.publicKeyParameters = (SM2PublicKeyParameters) pk;
    }

    public SM2p256v1PrivateCrtKey(BigInteger x, BigInteger y, BigInteger d) {
        super(new SM2PrivateKeyParameters(d));
        this.publicKeyParameters = new SM2PublicKeyParameters(x, y);
    }

    private final SM2PublicKeyParameters publicKeyParameters;

    public static SM2p256v1PrivateCrtKey getInstance(Object o) {
        if (null == o) throw new IllegalArgumentException("getInstance with Null of Key");
        ASN1Sequence kb = ASN1Sequence.getInstance(o);
        //Version
        Enumeration pvkInfo = kb.getObjects();
        SM2p256v1PublicKey pubKey = null;
        BigInteger d = null;
        int version = -1;
        boolean isPKCS8 = false;
        while (pvkInfo.hasMoreElements()) {
            Object asn = pvkInfo.nextElement();
            if (asn instanceof ASN1Integer) {
                // version
                if (((ASN1Integer) asn).getValue().intValue() != 1 && isPKCS8) {
                    throw new IllegalArgumentException("Not Support Version of Key");
                } else if (version >= 0) {
                    d = ((ASN1Integer) asn).getValue();
                } else {
                    version = ((ASN1Integer) asn).getValue().intValue();
                }
            } else if (asn instanceof DEROctetString) {
                //VK PKCS#8
                d = BigIntegers.fromUnsignedByteArray(((DEROctetString) asn).getOctets());
            } else if (asn instanceof DERTaggedObject) {
                // PKCS#8
                // param & public key
                if (((DERTaggedObject) asn).getTagNo() == 0) {
                    //parameters
                    //skip
                } else if (((DERTaggedObject) asn).getTagNo() == 1) {
                    // publicKey
                    pubKey = SM2p256v1PublicKey.getInstance(
                            DERBitString.getInstance(((DERTaggedObject) asn).getObject()));
                } else {
                    //skip
                }

            } else if (asn instanceof AlgorithmIdentifier) {
                if (((AlgorithmIdentifier) asn).getObjectId().equals(GMTObjectIdentifiers.id_sm2)) {
                    isPKCS8 = true;
                }
            } else if (!isPKCS8 && asn instanceof DERBitString) {
                pubKey = SM2p256v1PublicKey.getInstance(asn);
            }
        }
        if (pubKey == null || d == null)
            throw new IllegalArgumentException("Parse SM2 Private CertKey Failed Need(GMT0010) or PKCS#8");
        return new SM2p256v1PrivateCrtKey(
                pubKey.getQ().getXCoord().toBigInteger(),
                pubKey.getQ().getYCoord().toBigInteger(),
                d);
    }

    public String toString() {
        return String.format("SM2 Private Cert Key: \tXCoord->%s \tYCoord->%s \tD->%s",
                             publicKeyParameters.getQ().getXCoord().toString(),
                             publicKeyParameters.getQ().getYCoord().toString(),
                             privateKeyParameters.getD().toString(16)
        );
    }

    /**
     * return the encoding format we produce in getEncoded().
     *
     * @return the encoding format we produce in getEncoded().
     */
    public String getFormat() {
        return "PKCS#8";
    }

    /**
     * Return a PKCS8 representation of the key. The sequence returned
     * represents a full PrivateKeyInfo object.
     *
     * @return a PKCS8 representation of the key.
     */
    public byte[] getEncoded() {
        //GMT 0010
        return new BCECPrivateKey(
                "SM2",
                new ECPrivateKeySpec(getD(), ECNamedCurveTable.getParameterSpec("SM2")),
                KJHXTCProvider.CONFIGURATION
        ).getEncoded();
        //        ASN1EncodableVector v = new ASN1EncodableVector();
//        v.add(new ASN1Integer(1));
//        v.add(new ASN1Integer(privateKeyParameters.getD()));
//        v.add(new DERBitString(publicKeyParameters.getQ().getEncoded(false)));
//        try {
//            return new DERSequence(v).getEncoded();
//        } catch (IOException e) {
//            throw new IllegalStateException(e);
//        }
    }

    public ECPoint getQ() {
        return publicKeyParameters.getQ();
    }

    public BigInteger getD() {
        return privateKeyParameters.getD();
    }
}
