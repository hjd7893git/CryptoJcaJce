package com.kjhxtc.jcajce.provider.asymmetric.sm2;

import com.kjhxtc.crypto.params.AsymmetricKeyParameter;
import com.kjhxtc.crypto.params.SM2PrivateKeyParameters;
import com.kjhxtc.jce.interfaces.ECPrivateKey;
import com.kjhxtc.jce.spec.ECParameterSpec;
import com.kjhxtc.util.BigIntegers;

import java.math.BigInteger;

/**
 * @deprecated use {@link com.kjhxtc.jcajce.provider.asymmetric.ec.BCECPrivateKey} instead
 */
@Deprecated
public class SM2p256v1PrivateKey implements ECPrivateKey {
    private static final long serialVersionUID = 5220577486987620366L;

    public SM2p256v1PrivateKey(AsymmetricKeyParameter vk) {
        if (!vk.isPrivate() || !(vk instanceof SM2PrivateKeyParameters))
            throw new IllegalArgumentException("SM2 Private key required");
        privateKeyParameters = (SM2PrivateKeyParameters) vk;
    }

    public SM2p256v1PrivateKey(BigInteger d) {
        privateKeyParameters = new SM2PrivateKeyParameters(d);
    }

    final SM2PrivateKeyParameters privateKeyParameters;

    public String getAlgorithm() {
        return "SM2";
    }

    public String getFormat() {
        return "RAW";
    }

    public byte[] getEncoded() {
        return BigIntegers.asUnsignedByteArray(32, privateKeyParameters.getD());
    }

    public String toString() {
        return String.format("SM2 Private Key: \tD->%s", privateKeyParameters.getD().toString(16)
        );
    }

    public BigInteger getD() {
        return privateKeyParameters.getD();
    }

    public ECParameterSpec getParameters() {
        return new ECParameterSpec(
                privateKeyParameters.getParameters().getCurve(),
                privateKeyParameters.getParameters().getG(),
                privateKeyParameters.getParameters().getN(),
                privateKeyParameters.getParameters().getH(),
                privateKeyParameters.getParameters().getSeed()
        );
    }
}
