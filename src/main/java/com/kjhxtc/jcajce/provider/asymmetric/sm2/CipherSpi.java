package com.kjhxtc.jcajce.provider.asymmetric.sm2;

import com.kjhxtc.asn1.cngmt.SM2Cipher;
import com.kjhxtc.crypto.AsymmetricBlockCipher;
import com.kjhxtc.crypto.CipherParameters;
import com.kjhxtc.crypto.InvalidCipherTextException;
import com.kjhxtc.crypto.engines.SM2Engine;
import com.kjhxtc.crypto.params.*;
import com.kjhxtc.jcajce.provider.asymmetric.util.ECUtil;
import com.kjhxtc.util.Arrays;
import com.kjhxtc.util.BigIntegers;

import javax.crypto.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.*;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.AlgorithmParameterSpec;

public class CipherSpi extends javax.crypto.CipherSpi {
    protected boolean forEncrypt;
    private AsymmetricBlockCipher engine;
    private ByteArrayOutputStream outputStream;

    protected void engineSetMode(String s) throws NoSuchAlgorithmException {
        if (null == s || s.equals("") || s.equalsIgnoreCase("NONE") || s.equalsIgnoreCase("ECB")) {
            return;
        }
        throw new IllegalArgumentException("Not Support Mode `" + s + "`");
    }

    protected void engineSetPadding(String s) throws NoSuchPaddingException {
        if (null == s || s.equals("") || s.equalsIgnoreCase("NONE") || s.equalsIgnoreCase("NOPADDING")) {
            return;
        }
        throw new IllegalArgumentException("Not Support Padding `" + s + "`");
    }

    protected int engineGetBlockSize() {
        return 0;
    }

    protected int engineGetOutputSize(int i) {
        return outputStream.size();
    }

    protected byte[] engineGetIV() {
        return new byte[0];
    }

    protected AlgorithmParameters engineGetParameters() {
        return null;
    }

    protected void engineInit(int i, Key key, SecureRandom secureRandom) throws InvalidKeyException {
        CipherParameters parameters;

        if (i == Cipher.ENCRYPT_MODE) {
            forEncrypt = true;
            if (key instanceof SM2p256v1PublicKey) {
                parameters = new SM2PublicKeyParameters(((SM2p256v1PublicKey) key).getQ());
            } else {
                ECPublicKeyParameters parameter = (ECPublicKeyParameters) ECUtil.generatePublicKeyParameter((ECPublicKey) key);
                parameters = new SM2PublicKeyParameters(parameter.getQ());
            }
        } else if (i == Cipher.DECRYPT_MODE) {
            forEncrypt = false;
            if (key instanceof SM2p256v1PrivateKey) {
                parameters = new SM2PrivateKeyParameters(((SM2p256v1PrivateKey) key).getD());
            } else {
                ECPrivateKeyParameters parameter = (ECPrivateKeyParameters) ECUtil.generatePrivateKeyParameter((ECPrivateKey) key);
                parameters = new SM2PrivateKeyParameters(parameter.getD());
            }
        } else {
            throw new InvalidKeyException("Encrypt with BCECPublicKey and Decrypt with BCECPrivateKey");
        }

        engine = new SM2Engine();
        engine.init(forEncrypt, new ParametersWithRandom(parameters, secureRandom));
        outputStream = new ByteArrayOutputStream(128);
    }

    protected void engineInit(int i, Key key, AlgorithmParameterSpec algorithmParameterSpec, SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        //TODO
        throw new InvalidAlgorithmParameterException("Not Impl");
    }

    protected void engineInit(int i, Key key, AlgorithmParameters algorithmParameters, SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        //TODO
        throw new InvalidAlgorithmParameterException("Not Impl");
    }

    protected byte[] engineUpdate(byte[] bytes, int i, int i1) {
        outputStream.write(bytes, i, i1);
        return null;
    }

    protected int engineUpdate(byte[] bytes, int i, int i1, byte[] bytes1, int i2) throws ShortBufferException {
        outputStream.write(bytes, i, i1);
        return 0;
    }

    protected byte[] engineDoFinal(byte[] bytes, int i, int i1) throws IllegalBlockSizeException, BadPaddingException {
        try {
            outputStream.write(bytes, i, i1);
            final byte[] fullData = outputStream.toByteArray();
            byte[] tmpBuf;
            byte[] outBuf;
            if (forEncrypt) {
                tmpBuf = engine.processBlock(fullData, 0, fullData.length);
                SM2Cipher cipherText = new SM2Cipher(
                        BigIntegers.fromUnsignedByteArray(tmpBuf, 0, 32),
                        BigIntegers.fromUnsignedByteArray(tmpBuf, 32, 32),
                        Arrays.copyOfRange(tmpBuf, 64, 96),
                        Arrays.copyOfRange(tmpBuf, 96, tmpBuf.length)
                );
                outBuf = cipherText.getEncoded("DER");
            } else {
                SM2Cipher cipherText = SM2Cipher.getInstance(fullData);
                if (cipherText == null)
                    throw new IllegalBlockSizeException("Cipher Data Format Invalid");
                tmpBuf = Arrays.concatenate(BigIntegers.asUnsignedByteArray(32, cipherText.getXCoordinate()),
                                            BigIntegers.asUnsignedByteArray(32, cipherText.getYCoordinate()));
                tmpBuf = Arrays.concatenate(tmpBuf, cipherText.getDigest(), cipherText.getCipherText());
                outBuf = engine.processBlock(tmpBuf, 0, tmpBuf.length);
            }
            outputStream.close();
            return outBuf;
        } catch (InvalidCipherTextException e) {
            throw new IllegalStateException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    protected int engineDoFinal(byte[] bytes, int i, int i1, byte[] bytes1, int i2) throws ShortBufferException, IllegalBlockSizeException, BadPaddingException {
        try {
            byte[] outBuf = engineDoFinal(bytes, i, i1);
            if (outBuf.length > bytes1.length - i2) {
                throw new ShortBufferException(String.format("Required %d Bytes Buffer Space", outBuf.length));
            }
            System.arraycopy(outBuf, 0, bytes1, i2, outBuf.length);
            outputStream.close();
            return outBuf.length;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}