package com.kjhxtc.jcajce.provider.asymmetric.sm2;

import com.kjhxtc.asn1.cngmt.SM2Signature;
import com.kjhxtc.crypto.CryptoException;
import com.kjhxtc.crypto.Digest;
import com.kjhxtc.crypto.digests.SM3Digest;
import com.kjhxtc.crypto.params.ECPrivateKeyParameters;
import com.kjhxtc.crypto.params.ECPublicKeyParameters;
import com.kjhxtc.crypto.params.SM2PrivateKeyParameters;
import com.kjhxtc.crypto.params.SM2PublicKeyParameters;
import com.kjhxtc.crypto.signers.SM2DigestSigner;
import com.kjhxtc.jcajce.provider.asymmetric.util.ECUtil;
import com.kjhxtc.util.Arrays;
import com.kjhxtc.util.BigIntegers;

import java.io.IOException;
import java.security.*;

public class SignatureSpi extends java.security.SignatureSpi {
    private SM2DigestSigner digestSigner;

    protected SignatureSpi(Digest digest, byte[] userID) {
        this.digestSigner = new SM2DigestSigner(digest, userID);
    }

    protected void engineInitVerify(PublicKey publicKey) throws InvalidKeyException {
        if (publicKey instanceof SM2p256v1PublicKey) {
            digestSigner.init(false, new SM2PublicKeyParameters(((SM2p256v1PublicKey) publicKey).getQ()));
        } else {
            ECPublicKeyParameters parameters = (ECPublicKeyParameters) ECUtil.generatePublicKeyParameter(publicKey);
            digestSigner.init(false, new SM2PublicKeyParameters(parameters.getQ()));
        }
    }

    protected void engineInitSign(PrivateKey privateKey) throws InvalidKeyException {
        if (privateKey instanceof SM2p256v1PrivateKey) {
            digestSigner.init(true, ((SM2p256v1PrivateKey) privateKey).privateKeyParameters);
        } else {
            ECPrivateKeyParameters parameters = (ECPrivateKeyParameters) ECUtil.generatePrivateKeyParameter(privateKey);
            digestSigner.init(true, new SM2PrivateKeyParameters(parameters.getD()));
        }
    }

    protected void engineUpdate(byte b) throws SignatureException {
        digestSigner.update(b);
    }

    protected void engineUpdate(byte[] bytes, int i, int i1) throws SignatureException {
        digestSigner.update(bytes, i, i1);
    }

    protected byte[] engineSign() throws SignatureException {
        try {
            // 因为底层实现格式非通用格式，需要在此处做转换
            byte[] sign = digestSigner.generateSignature();
            return new SM2Signature(
                    BigIntegers.fromUnsignedByteArray(sign, 0, 32),
                    BigIntegers.fromUnsignedByteArray(sign, 32, 32)
            ).getEncoded("DER");
        } catch (CryptoException e) {
            throw new SignatureException(e);
        } catch (IOException e) {
            throw new SignatureException(e);
        }
    }

    protected boolean engineVerify(byte[] bytes) throws SignatureException {
        // 因为底层实现格式非通用格式，需要在此处做转换
        SM2Signature sign;
        try {
            sign = SM2Signature.getInstance(bytes);
            if (null == sign) { //格式转换失败，说明签名格式不正确
                throw new IllegalArgumentException("Signature Value Format Invalid");
            }
        } catch (Exception e) {
            throw new SignatureException(e);
        }

        return digestSigner.verifySignature(Arrays.concatenate(
                BigIntegers.asUnsignedByteArray(32, sign.getR()),
                BigIntegers.asUnsignedByteArray(32, sign.getS())
        ));
    }

    protected void engineSetParameter(String s, Object o) throws InvalidParameterException {
        throw new UnsupportedOperationException("engineSetParameter unsupported");
    }

    protected Object engineGetParameter(String s) throws InvalidParameterException {
        throw new UnsupportedOperationException("engineGetParameter unsupported");
    }

    public static class sm3 extends SignatureSpi {
        static final byte[] defaultId = "1234567812345678".getBytes();

        public sm3() {
            super(new SM3Digest(), defaultId);
        }
    }
}
