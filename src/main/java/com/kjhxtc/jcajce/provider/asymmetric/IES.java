package com.kjhxtc.jcajce.provider.asymmetric;

import com.kjhxtc.jcajce.provider.config.ConfigurableProvider;
import com.kjhxtc.jcajce.provider.util.AsymmetricAlgorithmProvider;

public class IES
{
    private static final String PREFIX = "com.kjhxtc.jcajce.provider.asymmetric" + ".ies.";

    public static class Mappings
        extends AsymmetricAlgorithmProvider
    {
        public Mappings()
        {
        }

        public void configure(ConfigurableProvider provider)
        {
            provider.addAlgorithm("AlgorithmParameters.IES", PREFIX + "AlgorithmParametersSpi");
            provider.addAlgorithm("Cipher.IES", PREFIX + "CipherSpi$IES");
        }
    }
}
