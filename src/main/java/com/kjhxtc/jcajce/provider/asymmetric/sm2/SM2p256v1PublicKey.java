package com.kjhxtc.jcajce.provider.asymmetric.sm2;

import com.kjhxtc.asn1.DERBitString;
import com.kjhxtc.crypto.params.AsymmetricKeyParameter;
import com.kjhxtc.crypto.params.SM2PublicKeyParameters;
import com.kjhxtc.jce.interfaces.ECPublicKey;
import com.kjhxtc.jce.spec.ECParameterSpec;
import com.kjhxtc.math.ec.ECPoint;

import java.math.BigInteger;

/**
 * @deprecated use {@link com.kjhxtc.jcajce.provider.asymmetric.ec.BCECPublicKey} instead
 */
@Deprecated
public class SM2p256v1PublicKey implements ECPublicKey {
    private static final long serialVersionUID = 4101299871190559507L;

    public SM2p256v1PublicKey(AsymmetricKeyParameter pk) {
        if (pk.isPrivate() || !(pk instanceof SM2PublicKeyParameters))
            throw new IllegalArgumentException("SM2 Public key required");
        publicKeyParameters = (SM2PublicKeyParameters) pk;
    }

    public SM2p256v1PublicKey(BigInteger x, BigInteger y) {
        publicKeyParameters = new SM2PublicKeyParameters(x, y);
    }

    public SM2p256v1PublicKey(byte[] encoded) {
        publicKeyParameters = new SM2PublicKeyParameters(encoded);
    }

    public static SM2p256v1PublicKey getInstance(Object o) {
        DERBitString pkDer = DERBitString.getInstance(o);
        return new SM2p256v1PublicKey(pkDer.getBytes());
    }

    private final SM2PublicKeyParameters publicKeyParameters;

    public String getAlgorithm() {
        return "SM2";
    }

    public String getFormat() {
        return "RAW";
    }

    public byte[] getEncoded() {
        return publicKeyParameters.getQ().getEncoded();
    }

    public String toString() {
        return String.format("SM2 Public Key: \tXCoord->%s \tYCoord->%s",
                publicKeyParameters.getQ().getXCoord().toString(),
                publicKeyParameters.getQ().getYCoord().toString()
        );
    }

    public ECPoint getQ() {
        return publicKeyParameters.getQ();
    }

    public ECParameterSpec getParameters() {
        return new ECParameterSpec(
                publicKeyParameters.getParameters().getCurve(),
                publicKeyParameters.getParameters().getG(),
                publicKeyParameters.getParameters().getN(),
                publicKeyParameters.getParameters().getH(),
                publicKeyParameters.getParameters().getSeed()
        );
    }
}
