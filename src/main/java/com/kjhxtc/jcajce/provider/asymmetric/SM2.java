package com.kjhxtc.jcajce.provider.asymmetric;

import com.kjhxtc.asn1.cngmt.GMTObjectIdentifiers;
import com.kjhxtc.jcajce.provider.asymmetric.ec.KeyFactorySpi;
import com.kjhxtc.jcajce.provider.config.ConfigurableProvider;
import com.kjhxtc.jcajce.provider.util.AsymmetricAlgorithmProvider;

public class SM2 {
    private static final String PREFIX = "com.kjhxtc.jcajce.provider.asymmetric" + ".sm2.";

    public static class Mappings
            extends AsymmetricAlgorithmProvider {
        public Mappings() {
        }

        public void configure(ConfigurableProvider provider) {
            //provider.addAlgorithm("AlgorithmParameterGenerator.SM2", PREFIX + "AlgorithmParameterGeneratorSpi");
            provider.addAlgorithm("AlgorithmParameters.SM2", PREFIX + "AlgorithmParametersSpi");

            provider.addAlgorithm("Cipher.SM2", PREFIX + "CipherSpi");
            provider.addAlgorithm("Alg.Alias.Cipher.SM2/ECB/NOPADDING", "CipherSpi");
            provider.addAlgorithm("Alg.Alias.Cipher.SM2/NONE/NOPADDING", "CipherSpi");
            provider.addAlgorithm("Alg.Alias.Cipher.SM2/NONE/NONE", "CipherSpi");

            provider.addAlgorithm("KeyFactory.SM2", "com.kjhxtc.jcajce.provider.asymmetric" + ".ec." + "KeyFactorySpi$SM2");
            provider.addAlgorithm("KeyPairGenerator.SM2", "com.kjhxtc.jcajce.provider.asymmetric" + ".ec." + "KeyPairGeneratorSpi$SM2");

            provider.addAlgorithm("KeyAgreement.SM2withSM3KDF", PREFIX + "KeyAgreementSpi");


            registerOid(provider, GMTObjectIdentifiers.id_sm2, "SM2", new KeyFactorySpi.SM2());


            provider.addAlgorithm("Signature.SM3WithSM2", PREFIX + "SignatureSpi");
            provider.addAlgorithm("Signature.SM2WithSM3", PREFIX + "SignatureSpi");

            addSignatureAlgorithm(provider, "SM3", "SM2", PREFIX + "SignatureSpi$sm3", GMTObjectIdentifiers.sm3WithSM2);
        }
    }
}
