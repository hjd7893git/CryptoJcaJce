package com.kjhxtc.jcajce.provider.asymmetric.sm2;

import com.kjhxtc.crypto.digests.SM3Digest;
import com.kjhxtc.crypto.engines.SM2Core;
import com.kjhxtc.jcajce.spec.SM2kaParameterSpec;
import com.kjhxtc.jce.interfaces.ECPublicKey;
import com.kjhxtc.math.ec.ECPoint;

import javax.crypto.SecretKey;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.*;
import java.security.spec.AlgorithmParameterSpec;

public class KeyAgreementSpi
        extends javax.crypto.KeyAgreementSpi {
    private SM2p256v1PrivateCrtKey localPrivateCrtKey;
    private SM2p256v1PrivateCrtKey localTempKey;
    private SM2p256v1PublicKey peerPublicKey;

    private SM2kaParameterSpec parameterSpec;

    static BigInteger getW() {
        return BigInteger.valueOf(127L);
    }

    protected void engineInit(Key key, SecureRandom secureRandom) throws InvalidKeyException {
        throw new IllegalStateException("Not Permit, Use others");
    }

    protected void engineInit(Key key, AlgorithmParameterSpec algorithmParameterSpec, SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        if (key instanceof SM2p256v1PrivateCrtKey) {
            localPrivateCrtKey = (SM2p256v1PrivateCrtKey) key;
            KeyPairGeneratorSpi kg = new KeyPairGeneratorSpi();
            kg.initialize(256, secureRandom);
            KeyPair keyPair = kg.generateKeyPair();
            localTempKey = (SM2p256v1PrivateCrtKey) keyPair.getPrivate();
        } else {
            throw new InvalidKeyException("SM2p256v1PrivateCrtKey Required");
        }
        if (algorithmParameterSpec instanceof SM2kaParameterSpec) {
            parameterSpec = (SM2kaParameterSpec) algorithmParameterSpec;
        } else {
            throw new InvalidParameterException("SM2kaParameterSpec Required");
        }
    }

    protected Key engineDoPhase(Key key, boolean b) throws InvalidKeyException, IllegalStateException {
        if (null == localPrivateCrtKey || null == localTempKey) throw new IllegalStateException("engineInit first");
        if (!b) {
            //stag 1 Receive PublicKey From Peer, return Local Temp Public Key
            if (key instanceof SM2p256v1PublicKey) {
                peerPublicKey = (SM2p256v1PublicKey) key;
                return new SM2p256v1PublicKey(localTempKey.getQ().getEncoded());
            }
            throw new InvalidKeyException("Not EC Public Key");
        }
        //stag 2 Receive Peer Temp Public Key, generate Session Key
        if (null == peerPublicKey) throw new IllegalStateException("engineDoPhase required step 1");
        if (key instanceof ECPublicKey) {
            BigInteger w = getW();
            BigInteger pow = BigInteger.valueOf(2L).pow(w.intValue());

            ECPublicKey peerTempKey = (ECPublicKey) key;
            BigInteger xLocal = pow.add(
                    localTempKey.getQ().getXCoord().toBigInteger().and(pow.subtract(BigInteger.ONE))
            );
            BigInteger tLocal = localPrivateCrtKey.getD().add(xLocal.multiply(localTempKey.getD())).mod(SM2Core.getN());
            BigInteger xPeer = pow.add(
                    peerTempKey.getQ().getXCoord().toBigInteger().and(pow.subtract(BigInteger.ONE))
            );
            ECPoint point = peerPublicKey.getQ()
                    .add(peerTempKey.getQ().multiply(xPeer).normalize()).normalize()
                    .multiply(tLocal.multiply(SM2Core.getH())).normalize();
            if (point.isInfinity()) throw new IllegalStateException("failed");

            ByteArrayOutputStream of = new ByteArrayOutputStream();
            try {
                of.write(point.getEncoded(), 1, 64);
                if (!parameterSpec.isPassive()) {
                    of.write(SM2Core.getSignerA_Z(parameterSpec.getID(), localPrivateCrtKey.getQ()));
                    of.write(SM2Core.getSignerA_Z(parameterSpec.getPeerID(), peerPublicKey.getQ()));
                } else {
                    of.write(SM2Core.getSignerA_Z(parameterSpec.getPeerID(), peerPublicKey.getQ()));
                    of.write(SM2Core.getSignerA_Z(parameterSpec.getID(), localPrivateCrtKey.getQ()));
                }
            } catch (IOException io) {
                throw new IllegalStateException(io);
            }
            this.sharedKey = new SecretKeySpec(SM2Core.kdf(of.toByteArray(), 16, new SM3Digest()), "SM4");
            return null;
        }
        throw new InvalidKeyException("Not EC Public Key");
    }

    private SecretKeySpec sharedKey;

    protected byte[] engineGenerateSecret() throws IllegalStateException {
        return sharedKey.getEncoded();
    }

    protected int engineGenerateSecret(byte[] bytes, int i) throws IllegalStateException, ShortBufferException {
        if (null == bytes || bytes.length - i < sharedKey.getEncoded().length) throw new ShortBufferException();
        byte[] key = sharedKey.getEncoded();
        for (int j = 0; j < key.length; j++) {
            bytes[i + j] = key[i];
        }
        return key.length;
    }

    protected SecretKey engineGenerateSecret(String s) throws IllegalStateException, NoSuchAlgorithmException, InvalidKeyException {
        return new SecretKeySpec(sharedKey.getEncoded(), s);
    }
}