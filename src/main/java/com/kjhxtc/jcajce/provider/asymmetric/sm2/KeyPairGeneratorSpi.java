package com.kjhxtc.jcajce.provider.asymmetric.sm2;

import com.kjhxtc.crypto.AsymmetricCipherKeyPair;
import com.kjhxtc.crypto.engines.SM2Core;
import com.kjhxtc.crypto.generators.SM2KeyPairGenerator;
import com.kjhxtc.crypto.params.ECKeyGenerationParameters;

import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;

/**
 * @deprecated use {@link com.kjhxtc.jcajce.provider.asymmetric.ec.KeyPairGeneratorSpi.SM2 } instead
 */
@Deprecated
public class KeyPairGeneratorSpi extends java.security.KeyPairGenerator {
    private SM2KeyPairGenerator engine;

    protected KeyPairGeneratorSpi(String s) {
        super(s);
    }

    public KeyPairGeneratorSpi() {
        super("SM2");
        engine = new SM2KeyPairGenerator();
    }

    public void initialize(
            int strength,
            SecureRandom random) {
        if (strength != 256) throw new IllegalArgumentException("SM2 now Only support 256bits");
        engine.init(new ECKeyGenerationParameters(SM2Core.getECDomainParameters(), random));
    }

    public void initialize(
            AlgorithmParameterSpec params,
            SecureRandom random)
            throws InvalidAlgorithmParameterException {
        if (params instanceof ECKeyGenerationParameters) {
            engine.init((ECKeyGenerationParameters) params);
        } else {
            engine.init(new ECKeyGenerationParameters(SM2Core.getECDomainParameters(), random));
        }
    }

    public KeyPair generateKeyPair() {
        AsymmetricCipherKeyPair pair = engine.generateKeyPair();
        return new KeyPair(
                new SM2p256v1PublicKey(pair.getPublic()),
                new SM2p256v1PrivateCrtKey(pair.getPrivate(), pair.getPublic())
        );
    }

}