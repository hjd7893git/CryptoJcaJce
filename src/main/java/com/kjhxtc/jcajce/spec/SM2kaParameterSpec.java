package com.kjhxtc.jcajce.spec;

import java.security.spec.AlgorithmParameterSpec;

public class SM2kaParameterSpec
        implements AlgorithmParameterSpec {
    private final boolean isPassive;
    private final byte[] ID;
    private final byte[] peerID;

    public SM2kaParameterSpec(boolean isServer, byte[] id, byte[] peerId) {
        isPassive = isServer;
        ID = id.clone();
        peerID = peerId.clone();
    }

    public byte[] getPeerID() {
        return peerID;
    }

    public boolean isPassive() {
        return isPassive;
    }

    public byte[] getID() {
        return ID;
    }
}
