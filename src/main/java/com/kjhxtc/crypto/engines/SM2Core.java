package com.kjhxtc.crypto.engines;

import com.kjhxtc.asn1.cngmt.GMTNamedCurves;
import com.kjhxtc.asn1.x9.X9ECParameters;
import com.kjhxtc.crypto.DerivationParameters;
import com.kjhxtc.crypto.Digest;
import com.kjhxtc.crypto.DigestDerivationFunction;
import com.kjhxtc.crypto.digests.SM3Digest;
import com.kjhxtc.crypto.generators.KDF2BytesGenerator;
import com.kjhxtc.crypto.params.ECDomainParameters;
import com.kjhxtc.crypto.params.ISO18033KDFParameters;
import com.kjhxtc.crypto.util.encoder.Hex;
import com.kjhxtc.math.ec.ECPoint;

import java.math.BigInteger;
import java.util.Random;

/**
 * SM2 256Bits
 */
public class SM2Core {

    //坐标参数
    private static final X9ECParameters Params = GMTNamedCurves.SM2p256v1Parameters.getParameters();

    public static ECPoint createPoint(BigInteger x, BigInteger y) {
        return Params.getCurve().createPoint(x, y);
    }

    public static ECPoint createPoint(byte[] encoded) {
        return Params.getCurve().decodePoint(encoded);
    }

    public static BigInteger getA() {
        return Params.getCurve().getA().toBigInteger();
    }

    public static BigInteger getB() {
        return Params.getCurve().getB().toBigInteger();
    }

    public static BigInteger getN() {
        return Params.getN();
    }

    public static BigInteger getH() {
        return Params.getH();
    }

    public static ECPoint getG() {
        return Params.getG();
    }

    public static ECDomainParameters getECDomainParameters() {
        return new ECDomainParameters(Params.getCurve(), Params.getG(), Params.getN(), Params.getH());
    }

    /**
     * 256bits 随机数产生器
     */
    protected static BigInteger genRandomSeed(Random rdm) {
        BigInteger seed;
        do {
            seed = new BigInteger(256, rdm);
        } while (seed.compareTo(getN()) >= 0 && seed.compareTo(BigInteger.ONE) < 0);
        return seed;
    }

    protected static ECPoint getEcPoint(BigInteger k) {
        return Params.getG().multiply(k).normalize();
    }

    protected static ECPoint getEcPoint(ECPoint Point, BigInteger k) {
        return Point.multiply(k);
    }

    /**
     * KDF
     *
     * @param Z      data
     * @param kLen   output Key length int bytes
     * @param digest MessageDigest
     * @return key
     */
    public static byte[] kdf(byte[] Z, int kLen, Digest digest) {
        DigestDerivationFunction a = new KDF2BytesGenerator(digest);
        byte[] chk = new byte[kLen];
        DerivationParameters dp = new ISO18033KDFParameters(Z);
        a.init(dp);
        a.generateBytes(chk, 0, chk.length);
        return chk;
    }

    /**
     * 预处理1: 签名者用户A计算用户A的杂凑值
     *
     * @param ID_A    用户A的参数
     * @param Point_A 用户A的点
     * @return HashOfSM3
     */
    public static byte[] getSignerA_Z(byte[] ID_A, ECPoint Point_A) {
        return getSignerA_Z(ID_A, Point_A, new SM3Digest());
    }

    public static byte[] getSignerA_Z(byte[] ID_A, ECPoint Point_A, Digest digest) {
        int id_length = ID_A.length * 8;
        byte[] entl_a = Hex.decode(String.format("%04x", id_length));

        //计算Z_A
        digest.reset();
        digest.update(entl_a, entl_a.length - 2, 2);    //ENTL_A
        digest.update(ID_A, 0, ID_A.length);            //ID_A
        byte[] bytesA = Hex.decode(String.format("%064X", getA()));
        byte[] bytesB = Hex.decode(String.format("%064X", getB()));
        digest.update(bytesA, 0, 32); //a
        digest.update(bytesB, 0, 32); //b
        //偏移1字节的 PC
        digest.update(getG().getEncoded(false), 1, 64); // (Gx,Gy)
        digest.update(Point_A.getEncoded(false), 1, 64); // (Ax,Ay)
        byte[] Z_A = new byte[digest.getDigestSize()];
        digest.doFinal(Z_A, 0);
        return Z_A;
    }

    /**
     * 预处理2: 使用预处理1结果Z和待签名数据M计算摘要值H
     *
     * @param Za      结果Z
     * @param message 待签名数据M
     * @return SM3摘要值M
     */
    public static BigInteger calculateE(byte[] Za, byte[] message) {
        return calculateE(Za, message, new SM3Digest());
    }

    public static BigInteger calculateE(byte[] Za, byte[] message, Digest digest) {
        digest.reset();
        digest.update(Za, 0, Za.length);
        digest.update(message, 0, message.length);
        byte[] e_byte = new byte[digest.getDigestSize()];
        digest.doFinal(e_byte, 0);
        String str_e = Hex.toHexString(e_byte);
        return new BigInteger(str_e, 16);
    }
}