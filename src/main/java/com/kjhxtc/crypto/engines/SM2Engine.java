package com.kjhxtc.crypto.engines;

import com.kjhxtc.crypto.AsymmetricBlockCipher;
import com.kjhxtc.crypto.CipherParameters;
import com.kjhxtc.crypto.Digest;
import com.kjhxtc.crypto.InvalidCipherTextException;
import com.kjhxtc.crypto.digests.SM3Digest;
import com.kjhxtc.crypto.params.ParametersWithRandom;
import com.kjhxtc.crypto.params.SM2KeyParameters;
import com.kjhxtc.crypto.params.SM2PrivateKeyParameters;
import com.kjhxtc.crypto.params.SM2PublicKeyParameters;
import com.kjhxtc.crypto.util.encoder.Hex;
import com.kjhxtc.math.ec.ECPoint;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

/**
 * SM2-256bits with SM3 For Basic Encrypt or Decrypt
 */
public class SM2Engine extends SM2Core implements AsymmetricBlockCipher {

    private SM2KeyParameters sm2key = null;
    private boolean isEncrypt;
    private Digest digest;
    private Random randomEngine;

    public SM2Engine() {
    }

    public int getInputBlockSize() {
        // SM2 NOT BLOCK CIPHER
        return 0;
    }

    public int getOutputBlockSize() {
        // SM2 NOT BLOCK CIPHER
        return 0;
    }

    /**
     * @param forEncryption 加密true/解密false标志
     * @param param         初始化公钥/私钥
     */
    public void init(
            boolean forEncryption,
            CipherParameters param) {
        isEncrypt = forEncryption;
        if (param instanceof ParametersWithRandom) {
            randomEngine = ((ParametersWithRandom) param).getRandom();
            sm2key = (SM2KeyParameters) ((ParametersWithRandom) param).getParameters();
        } else {
            randomEngine = new SecureRandom();
            sm2key = (SM2KeyParameters) param;
        }
        digest = new SM3Digest();
    }

    public byte[] processBlock(byte[] abyte0, int i, int j) throws InvalidCipherTextException {
        if (null == sm2key) throw new IllegalStateException("Not init");
        if (isEncrypt && (sm2key instanceof SM2PublicKeyParameters)) {
            ECPoint pub = ((SM2PublicKeyParameters) sm2key).getQ();
            return encrypt(abyte0, pub);
        } else if ((!isEncrypt) && (sm2key instanceof SM2PrivateKeyParameters)) {
            BigInteger seed_k = ((SM2PrivateKeyParameters) sm2key).getD();
            return decrypt(abyte0, seed_k);
        } else {
            throw new IllegalStateException("Only Support encrypt with Public Key|decrypt with Private Key");
        }
    }

    /**
     * SM2 加密
     * //use key -->GetZ;then kdf(Z,input.length)
     * //at last kdf_res Xor input
     *
     * @param input   待加密数据
     * @param Point_B 公钥PB坐标点
     * @return 加密结果 C1||C3||C2
     */
    public byte[] encrypt(byte[] input, ECPoint Point_B) {
        final BigInteger seed_num = genRandomSeed(randomEngine);
        final byte[] res = new byte[input.length + 32 + 32 + 32];

        //A2
        ECPoint pC1 = getEcPoint(seed_num);
        byte[] C1 = pC1.getEncoded(false);
        byte[] C3 = new byte[digest.getDigestSize()];

        //A3
        //A4 计算x2,y2
        ECPoint tmp = Point_B.multiply(seed_num);
        byte[] xy = new byte[64];
        System.arraycopy(tmp.getEncoded(), 1, xy, 0, 64);

        //A5
        //根据数据长度产生相应长度的数据
        byte[] tmp_key = kdf(xy, input.length, digest);
        //将明文XOR数据，得打加密数据C2
        for (int i = 0; i < input.length; i++) {
            res[96 + i] = (byte) (tmp_key[i] ^ input[i]);
        }

        //A6
        digest.reset();
        digest.update(xy, 0, 32);
        digest.update(input, 0, input.length);
        digest.update(xy, 32, 32);
        digest.doFinal(C3, 0);

        System.arraycopy(C1, 1, res, 0, 64);
        System.arraycopy(C3, 0, res, 64, 32);
        return res;
    }

    /**
     * SM2 解密
     * // use key -->GetZ;
     * // then kdf(Z,input.length)
     * // at last kdf_res Xor input
     *
     * @param input 待解密数据密文 C1||C3||C2
     * @param d     用户私钥
     * @return 解密后的明文数据
     */
    public byte[] decrypt(byte[] input, BigInteger d) throws InvalidCipherTextException {
        if (input == null || input.length <= 96) throw new InvalidCipherTextException("Cipher Message Invalid");
        final int cipher_len = input.length - 96;
        final byte[] C1 = new byte[64];
        final byte[] C2 = new byte[cipher_len];
        final byte[] C3 = new byte[32];
        final byte[] res = new byte[cipher_len];

        //A1 取出C1  C2 C3
        System.arraycopy(input, 0, C1, 0, 64);
        System.arraycopy(input, 96, C2, 0, cipher_len);
        System.arraycopy(input, 64, C3, 0, 32);

        BigInteger ix = new BigInteger(Hex.toHexString(C1, 0, 32), 16);
        BigInteger iy = new BigInteger(Hex.toHexString(C1, 32, 32), 16);

        //A3
        ECPoint tmp = createPoint(ix, iy).multiply(d);
        byte[] xy = new byte[64];
        System.arraycopy(tmp.getEncoded(), 1, xy, 0, 64);

        //A4 kdf
        byte[] tmp_key = kdf(xy, cipher_len, digest);
        String hex_key = Hex.toHexString(tmp_key);
        if (BigInteger.ZERO.compareTo(new BigInteger(hex_key, 16)) == 0) {
            throw new IllegalStateException("Random Key is Zero");
        }

        //A5
        for (int i = 0; i < res.length; i++) {
            res[i] = (byte) (tmp_key[i] ^ C2[i]);
        }

        //A6 计算哈希值
        digest.reset();
        digest.update(xy, 0, 32);
        digest.update(res, 0, res.length);
        digest.update(xy, 32, 32);

        byte[] tmpC3 = new byte[digest.getDigestSize()];
        digest.doFinal(tmpC3, 0);

        //校验最后的哈希值是否一致
        for (int i = 0; i < 32; i++) {
            if (tmpC3[i] != C3[i]) {
                throw new InvalidCipherTextException("Decrypt Failed,Message Digest Miss Match");
            }
        }
        return res;
    }
}
