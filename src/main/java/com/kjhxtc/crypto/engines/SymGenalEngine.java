package com.kjhxtc.crypto.engines;

import javax.crypto.IllegalBlockSizeException;

public interface SymGenalEngine {
	public static final Boolean ENCRYPT_MODE = true;
	public static final Boolean DECRYPT_MODE = false;
	public int getBlockSize();
	
	public void init(Boolean isEnc, byte[] key,  byte[] iv);
	public byte[] update(byte[] in) throws IllegalBlockSizeException;
	public byte[] doFinal(byte[] in) throws IllegalBlockSizeException;
}
