package com.kjhxtc.crypto.tls;

public interface TlsCredentials
{
    Certificate getCertificate();
}
