package com.kjhxtc.crypto.generators;

import com.kjhxtc.crypto.AsymmetricCipherKeyPair;
import com.kjhxtc.crypto.AsymmetricCipherKeyPairGenerator;
import com.kjhxtc.crypto.EphemeralKeyPair;
import com.kjhxtc.crypto.KeyEncoder;

public class EphemeralKeyPairGenerator
{
    private AsymmetricCipherKeyPairGenerator gen;
    private KeyEncoder keyEncoder;

    public EphemeralKeyPairGenerator(AsymmetricCipherKeyPairGenerator gen, KeyEncoder keyEncoder)
    {
        this.gen = gen;
        this.keyEncoder = keyEncoder;
    }

    public EphemeralKeyPair generate()
    {
        AsymmetricCipherKeyPair eph = gen.generateKeyPair();

        // Encode the ephemeral public key
         return new EphemeralKeyPair(eph, keyEncoder);
    }
}
