package com.kjhxtc.crypto.generators;

import com.kjhxtc.crypto.AsymmetricCipherKeyPair;
import com.kjhxtc.crypto.KeyGenerationParameters;
import com.kjhxtc.crypto.engines.SM2Core;
import com.kjhxtc.crypto.params.ECDomainParameters;
import com.kjhxtc.crypto.params.ECKeyGenerationParameters;
import com.kjhxtc.crypto.params.SM2PrivateKeyParameters;
import com.kjhxtc.crypto.params.SM2PublicKeyParameters;
import com.kjhxtc.math.ec.ECPoint;
import com.kjhxtc.math.ec.WNafUtil;

import java.math.BigInteger;
import java.security.SecureRandom;

public class SM2KeyPairGenerator extends ECKeyPairGenerator {
    private ECDomainParameters params;
    private SecureRandom random;

    public void init() {
        this.init(new ECKeyGenerationParameters(SM2Core.getECDomainParameters(), new SecureRandom()));
    }

    public void init(KeyGenerationParameters keygenerationparameters) {
        ECKeyGenerationParameters eckeygenerationparameters = (ECKeyGenerationParameters) keygenerationparameters;
        random = eckeygenerationparameters.getRandom();
        params = eckeygenerationparameters.getDomainParameters();
        if (random == null)
            random = new SecureRandom();
    }

    public AsymmetricCipherKeyPair generateKeyPair() {
        BigInteger biginteger = params.getN();
        int i = biginteger.bitLength();
        int j = i >>> 2;
        BigInteger bigInt1;
        do
            bigInt1 = new BigInteger(i, random);
        while (bigInt1.compareTo(TWO) < 0 || bigInt1.compareTo(biginteger) >= 0 || WNafUtil.getNafWeight(bigInt1) < j);
        ECPoint ecPoint = createBasePointMultiplier().multiply(params.getG(), bigInt1);
        return new AsymmetricCipherKeyPair(new SM2PublicKeyParameters(ecPoint), new SM2PrivateKeyParameters(bigInt1));
    }
}
