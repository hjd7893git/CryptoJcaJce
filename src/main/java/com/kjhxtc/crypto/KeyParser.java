package com.kjhxtc.crypto;

import java.io.IOException;
import java.io.InputStream;

import com.kjhxtc.crypto.params.AsymmetricKeyParameter;

public interface KeyParser
{
    AsymmetricKeyParameter readKey(InputStream stream)
        throws IOException;
}
