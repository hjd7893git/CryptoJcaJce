package com.kjhxtc.crypto.signers;

import com.kjhxtc.crypto.*;
import com.kjhxtc.crypto.digests.SM3Digest;
import com.kjhxtc.crypto.engines.SM2Core;
import com.kjhxtc.crypto.params.ParametersWithRandom;
import com.kjhxtc.crypto.params.SM2KeyParameters;
import com.kjhxtc.crypto.params.SM2PrivateKeyParameters;
import com.kjhxtc.crypto.params.SM2PublicKeyParameters;
import com.kjhxtc.math.ec.ECPoint;
import com.kjhxtc.util.Arrays;
import com.kjhxtc.util.BigIntegers;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

/**
 * SM2-256bits With SM3HashDigest Signature and Verify
 * <li>GM/T 0003</li>>
 * <li>GM/T 0009</li>>
 * <li>GM/T 0010</li>>
 */
public class SM2DigestSigner extends SM2Core implements Signer {

    private static final byte[] defaultUserID = "1234567812345678".getBytes();
    private SM2KeyParameters key;

    private boolean forSigning;
    private final byte[] userID;
    private ByteArrayOutputStream userData;
    private Digest digest;
    private Random randomEngine;

    /**
     * SM2 with SM3 Singer
     *
     * @param digest 哈希算法对象,null时默认sm3
     * @param UserID 用户ID,null是取默认
     */
    public SM2DigestSigner(Digest digest, byte[] UserID) {
        if (null != digest) {
            if (digest.getDigestSize() != 32) {
                throw new IllegalArgumentException("Digest Must be 256bits!");
            }
            this.digest = digest;
        } else {
            this.digest = new SM3Digest();
        }
        if (null != UserID) {
            this.userID = UserID;
        } else {
            this.userID = defaultUserID;
        }
        userData = new ByteArrayOutputStream();
    }


    public void init(boolean flag, CipherParameters parameters) {
        this.forSigning = flag;
        if (parameters instanceof ParametersWithRandom) {
            key = (SM2KeyParameters) ((ParametersWithRandom) parameters).getParameters();
            randomEngine = ((ParametersWithRandom) parameters).getRandom();
        } else {
            key = (SM2KeyParameters) parameters;
            randomEngine = new SecureRandom();
        }

        if (forSigning && !key.isPrivate()) {
            throw new IllegalArgumentException("signing requires private key");
        }

        if (!forSigning && key.isPrivate()) {
            throw new IllegalArgumentException("verification requires public key");
        }

        reset();
    }

    public void reset() {
        digest.reset();
        userData.reset();
    }

    /**
     * 待签名数据
     */
    public void update(byte byte0) {
        userData.write(byte0);
    }

    /**
     * 待签名数据
     */
    public void update(byte[] bytes, int i, int j) {
        userData.write(bytes, i, j);
    }

    public byte[] generateSignature() throws CryptoException,
            DataLengthException {
        if (!forSigning) {
            throw new IllegalStateException("SM2DigestSigner not initialised for signature generation.");
        }

        BigInteger d = ((SM2PrivateKeyParameters) key).getD();
        ECPoint PubPoint = getEcPoint(d);
        byte[] singer = getSignerA_Z(userID, PubPoint, digest);
        BigInteger[] rs = sign(singer, userData.toByteArray(), d);
        return Arrays.concatenate(BigIntegers.asUnsignedByteArray(32, rs[0]), BigIntegers.asUnsignedByteArray(32, rs[1]));
    }

    public boolean verifySignature(byte[] signature) {
        if (forSigning) {
            throw new IllegalStateException("SM2DigestSigner not initialised for verification");
        }
        if (signature == null || signature.length != 64) throw new IllegalArgumentException("signature Not R+S Format");
        ECPoint PA = ((SM2PublicKeyParameters) key).getQ();
        BigInteger r = BigIntegers.fromUnsignedByteArray(signature, 0, 32);
        BigInteger s = BigIntegers.fromUnsignedByteArray(signature, 32, 32);
        byte[] singer = getSignerA_Z(userID, PA, digest);
        return verify(r, s, PA, userData.toByteArray(), singer);
    }

    private BigInteger[] sign(byte[] singer, byte[] message, BigInteger d) throws CryptoException {
        if (message == null || message.length < 1) throw new DataLengthException("Message is Empty");
        BigInteger e = calculateE(singer, message, digest);
        BigInteger r;
        BigInteger s;
        BigInteger k;
        do {
            //A3
            k = genRandomSeed(randomEngine);
            //A4  根据随机数计算 x1
            ECPoint tmp = getEcPoint(k);
            //A5
            r = (e.add(tmp.getXCoord().toBigInteger())).mod(getN());
            //A6
            BigInteger tmp_da1 = BigInteger.ONE.add(d).modInverse(getN());
            BigInteger tmp_da2 = k.subtract(r.multiply(d));
            s = tmp_da1.multiply(tmp_da2).mod(getN());

        } while (r.compareTo(BigInteger.ZERO) == 0 ||
                r.add(k).compareTo(getN()) == 0 ||
                s.compareTo(BigInteger.ZERO) == 0);
        //A7
        return new BigInteger[]{r, s};
    }

    private Boolean verify(BigInteger sig_r, BigInteger sig_s, ECPoint PA, byte[] message, byte[] ZA) {
        // r' in the range [1,n-1]
        if (sig_r.compareTo(BigInteger.ONE) < 0 || sig_r.compareTo(getN()) >= 0) {
            return false;
        }
        // s' in the range [1,n-1]
        if (sig_s.compareTo(BigInteger.ONE) < 0 || sig_s.compareTo(getN()) >= 0) {
            return false;
        }
        //B3 B4
        BigInteger e = calculateE(ZA, message, digest);

        //B5
        BigInteger t = sig_r.add(sig_s).mod(getN());
        if (t.compareTo(BigInteger.ZERO) == 0) {
            return false;
        }
        //B6
        ECPoint G_s = getEcPoint(sig_s);
        ECPoint Pa_t = getEcPoint(PA, t);
        //B7
        BigInteger x1 = G_s.add(Pa_t).normalize().getXCoord().toBigInteger();
        BigInteger R = e.add(x1).mod(getN());
        return 0 == R.compareTo(sig_r);
    }
}
