package com.kjhxtc.crypto.ec;

import com.kjhxtc.crypto.CipherParameters;
import com.kjhxtc.math.ec.ECPoint;

public interface ECDecryptor
{
    void init(CipherParameters params);

    ECPoint decrypt(ECPair cipherText);
}
