package com.kjhxtc.crypto.ec;

import com.kjhxtc.crypto.CipherParameters;

public interface ECPairTransform
{
    void init(CipherParameters params);

    ECPair transform(ECPair cipherText);
}
