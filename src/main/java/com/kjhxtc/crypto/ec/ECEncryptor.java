package com.kjhxtc.crypto.ec;

import com.kjhxtc.crypto.CipherParameters;
import com.kjhxtc.math.ec.ECPoint;

public interface ECEncryptor
{
    void init(CipherParameters params);

    ECPair encrypt(ECPoint point);
}
