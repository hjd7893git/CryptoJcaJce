package com.kjhxtc.crypto.modes;

import com.kjhxtc.crypto.BlockCipher;
import com.kjhxtc.crypto.CipherParameters;
import com.kjhxtc.crypto.DataLengthException;
import com.kjhxtc.crypto.StreamBlockCipher;
import com.kjhxtc.crypto.params.ParametersWithIV;
import com.kjhxtc.crypto.util.encoder.Hex;
import com.kjhxtc.util.Arrays;

import java.io.ByteArrayOutputStream;

/**
 * implements a Cipher-FeedBack (CFB) mode on top of a simple cipher.
 */
public class CFBBlockCipher
    extends StreamBlockCipher
{
    private byte[]          IV;
    private byte[]          cfbV;
    private byte[]          cfbOutV;
    private byte[]          inBuf;

    private int             blockSize;
    private int             bitLength;
    private BlockCipher     cipher = null;
    private boolean         encrypting;
    private int             byteCount;

    /**
     * Basic constructor.
     *
     * @param cipher the block cipher to be used as the basis of the
     * feedback mode.
     * @param bitBlockSize the block size in bits (note: a multiple of 8)
     */
    public CFBBlockCipher(
        BlockCipher cipher,
        int         bitBlockSize)
    {
        super(cipher);

        this.cipher = cipher;
        this.blockSize = (bitBlockSize / 8) == 0 ? 1 : (bitBlockSize / 8);
        this.bitLength = bitBlockSize;
        //cfb1 cfb8 cfb
        if (bitLength > 8 && bitLength % 8 != 0) {
            throw new IllegalArgumentException("Not Support CFB" + bitLength);
        }
        if (bitLength < 8 && bitLength != 1) {
            throw new IllegalArgumentException("Not Support CFB" + bitLength);
        }

        this.IV = new byte[cipher.getBlockSize()];
        this.cfbV = new byte[cipher.getBlockSize()];
        this.cfbOutV = new byte[cipher.getBlockSize()];
        this.inBuf = new byte[blockSize];
    }

    /**
     * Initialise the cipher and, possibly, the initialisation vector (IV).
     * If an IV isn't passed as part of the parameter, the IV will be all zeros.
     * An IV which is too short is handled in FIPS compliant fashion.
     *
     * @param encrypting if true the cipher is initialised for
     *  encryption, if false for decryption.
     * @param params the key and other data required by the cipher.
     * @exception IllegalArgumentException if the params argument is
     * inappropriate.
     */
    public void init(
        boolean             encrypting,
        CipherParameters    params)
        throws IllegalArgumentException
    {
        this.encrypting = encrypting;
        
        if (params instanceof ParametersWithIV)
        {
            ParametersWithIV ivParam = (ParametersWithIV)params;
            byte[]      iv = ivParam.getIV();

            if (iv.length < IV.length)
            {
                // prepend the supplied IV with zeros (per FIPS PUB 81)
                System.arraycopy(iv, 0, IV, IV.length - iv.length, iv.length);
                for (int i = 0; i < IV.length - iv.length; i++)
                {
                    IV[i] = 0;
                }
            }
            else
            {
                System.arraycopy(iv, 0, IV, 0, IV.length);
            }

            reset();

            // if null it's an IV changed only.
            if (ivParam.getParameters() != null)
            {
                cipher.init(true, ivParam.getParameters());
            }
        }
        else
        {
            reset();

            // if it's null, key is to be reused.
            if (params != null)
            {
                cipher.init(true, params);
            }
        }
    }

    /**
     * return the algorithm name and mode.
     *
     * @return the name of the underlying algorithm followed by "/CFB"
     * and the block size in bits.
     */
    public String getAlgorithmName()
    {
        return cipher.getAlgorithmName() + "/CFB" + bitLength;
    }

    protected byte calculateByte(byte in)
          throws DataLengthException, IllegalStateException
    {
        return (encrypting) ? encryptByte(in) : decryptByte(in);
    }

    private byte encryptByte(byte in)
    {
        //I 1 = IV;
        //I j = LSB b-s (I j –1 ) | C# j -1   for j = 2 ... n;
        //O j = CIPH K (I j )                 for j = 1, 2 ... n;
        //C# j = P# j ⊕ MSB s (O j )          for j = 1, 2 ... n.

        byte rv = 0;
        if (bitLength == 1)
        { //cfb1
            for (int i = 7; i >= 0; i--)
            {
                cipher.processBlock(cfbV, 0, cfbOutV, 0);
                final byte mask = (byte) (0x01 << i);
                final byte oj = (byte) (cfbOutV[0] & 0x80);
                final byte pj = (byte) ((in & mask) << (7 - i));
                byte cj = (byte) ((byte) (oj ^ pj) & 0x80);
                rv = (byte) (rv | (byte) ((cj >>> (7 - i)) & mask));
                shiftLeft(cfbV, cj, cfbV);
            }
        }
        else
        {
            if (byteCount == 0)
            {
                cipher.processBlock(cfbV, 0, cfbOutV, 0);
            }

            rv = (byte) (cfbOutV[byteCount] ^ in);
            inBuf[byteCount++] = rv;

            if (byteCount == blockSize)
            {
                byteCount = 0;

                System.arraycopy(cfbV, blockSize, cfbV, 0, cfbV.length - blockSize);
                System.arraycopy(inBuf, 0, cfbV, cfbV.length - blockSize, blockSize);
            }
        }
        return rv;
    }

    private byte decryptByte(byte in)
    {
        //I 1 = IV;
        //I j = LSB b-s (I j -1 )| C# j -1                     for j = 2 ... n;
        //O j = CIPH K (I j )                                  for j = 1, 2 ... n;
        //P# j = C# j ⊕ MSB s (O j )                           for j = 1, 2 ... n.

        byte rv = 0;
        if (bitLength == 1)
        { //cfb1
            for (int i = 7; i >= 0; i--)
            {
                final byte mask = (byte) (0x01 << i);
                cipher.processBlock(cfbV, 0, cfbOutV, 0);
                final byte oj = (byte) (cfbOutV[0] & 0x80);
                final byte cj = (byte) ((in & mask) << (7 - i));
                final byte pj = (byte) ((byte) (oj ^ cj) & 0x80);
                rv = (byte) (rv | (byte) ((pj >>> (7 - i)) & mask));
                shiftLeft(cfbV, (byte) (in << (7 - i)), cfbV);
            }
        }
        else
        {
            if (byteCount == 0) {
                cipher.processBlock(cfbV, 0, cfbOutV, 0);
            }

            inBuf[byteCount] = in;
            rv = (byte) (cfbOutV[byteCount++] ^ in);

            if (byteCount == blockSize) {
                byteCount = 0;

                System.arraycopy(cfbV, blockSize, cfbV, 0, cfbV.length - blockSize);
                System.arraycopy(inBuf, 0, cfbV, cfbV.length - blockSize, blockSize);
            }
        }
        return rv;
    }

    /**
     * return the block size we are operating at.
     *
     * @return the block size we are operating at (in bytes).
     */
    public int getBlockSize()
    {
        return blockSize;
    }

    /**
     * Process one block of input from the array in and write it to
     * the out array.
     *
     * @param in the array containing the input data.
     * @param inOff offset into the in array the data starts at.
     * @param out the array the output data will be copied into.
     * @param outOff the offset into the out array the output will start at.
     * @exception DataLengthException if there isn't enough data in in, or
     * space in out.
     * @exception IllegalStateException if the cipher isn't initialised.
     * @return the number of bytes processed and produced.
     */
    public int processBlock(
        byte[]      in,
        int         inOff,
        byte[]      out,
        int         outOff)
        throws DataLengthException, IllegalStateException
    {
        processBytes(in, inOff, blockSize, out, outOff);

        return blockSize;
    }

    /**
     * Do the appropriate processing for CFB mode encryption.
     *
     * @param in the array containing the data to be encrypted.
     * @param inOff offset into the in array the data starts at.
     * @param out the array the encrypted data will be copied into.
     * @param outOff the offset into the out array the output will start at.
     * @exception DataLengthException if there isn't enough data in in, or
     * space in out.
     * @exception IllegalStateException if the cipher isn't initialised.
     * @return the number of bytes processed and produced.
     */
    public int encryptBlock(
        byte[]      in,
        int         inOff,
        byte[]      out,
        int         outOff)
        throws DataLengthException, IllegalStateException
    {
        processBytes(in, inOff, blockSize, out, outOff);

        return blockSize;
    }

    /**
     * Do the appropriate processing for CFB mode decryption.
     *
     * @param in the array containing the data to be decrypted.
     * @param inOff offset into the in array the data starts at.
     * @param out the array the encrypted data will be copied into.
     * @param outOff the offset into the out array the output will start at.
     * @exception DataLengthException if there isn't enough data in in, or
     * space in out.
     * @exception IllegalStateException if the cipher isn't initialised.
     * @return the number of bytes processed and produced.
     */
    public int decryptBlock(
        byte[]      in,
        int         inOff,
        byte[]      out,
        int         outOff)
        throws DataLengthException, IllegalStateException
    {
        processBytes(in, inOff, blockSize, out, outOff);

        return blockSize;
    }

    /**
     * Return the current state of the initialisation vector.
     *
     * @return current IV
     */
    public byte[] getCurrentIV()
    {
        return Arrays.clone(cfbV);
    }

    /**
     * reset the chaining vector back to the IV and reset the underlying
     * cipher.
     */
    public void reset()
    {
        System.arraycopy(IV, 0, cfbV, 0, IV.length);
        Arrays.fill(inBuf, (byte)0);
        byteCount = 0;

        cipher.reset();
    }

    private static void shiftLeft(byte[] in, byte pad, final byte[] out)
    {
        final ByteArrayOutputStream of = new ByteArrayOutputStream(in.length + 1);
        of.write(in, 0, in.length);
        of.write(pad);
        final byte[] oin = of.toByteArray();

        for (int i = 0; i < out.length; i++)
        {
            byte oi = 0;
            for (int j = 7; j >= 0; j--)
            {
                byte e = (byte) ((byte) (0x01 << j));
                byte t = (byte) (oin[i + (8 - j) / 8]);
                t = (byte) ((j == 0 ? t >>> 7 : t << 1) & e);
               oi = (byte) (oi | t);
            }
            out[i] = oi;
        }
    }
}
