package com.kjhxtc.crypto.prng;

import com.kjhxtc.crypto.prng.drbg.SP80090DRBG;

interface DRBGProvider
{
    SP80090DRBG get(EntropySource entropySource);
}
