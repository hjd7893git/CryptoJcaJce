package com.kjhxtc.crypto.prng;

public interface EntropySourceProvider
{
    EntropySource get(final int bitsRequired);
}
