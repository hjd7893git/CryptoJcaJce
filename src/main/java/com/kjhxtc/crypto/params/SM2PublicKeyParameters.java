package com.kjhxtc.crypto.params;

import com.kjhxtc.crypto.engines.SM2Core;
import com.kjhxtc.math.ec.ECPoint;

import java.math.BigInteger;

public class SM2PublicKeyParameters extends SM2KeyParameters {
    public SM2PublicKeyParameters(ECPoint ecpoint) {
        super(false, SM2Core.getECDomainParameters());
        Q = ecpoint.normalize();
    }

    public SM2PublicKeyParameters(BigInteger x, BigInteger y) {
        this(SM2Core.createPoint(x, y));
    }

    public SM2PublicKeyParameters(byte[] encoded) {
        this(SM2Core.createPoint(encoded));
    }

    public ECPoint getQ() {
        return Q;
    }

    private ECPoint Q;
}
