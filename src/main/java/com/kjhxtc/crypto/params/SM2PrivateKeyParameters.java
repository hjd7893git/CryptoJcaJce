package com.kjhxtc.crypto.params;

import com.kjhxtc.crypto.engines.SM2Core;

import java.math.BigInteger;

public class SM2PrivateKeyParameters extends SM2KeyParameters {
    public SM2PrivateKeyParameters(BigInteger biginteger) {
        super(true, SM2Core.getECDomainParameters());
        d = biginteger;
    }

    public BigInteger getD() {
        return d;
    }

    private BigInteger d;
}
