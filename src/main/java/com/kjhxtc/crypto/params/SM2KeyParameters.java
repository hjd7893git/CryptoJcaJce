package com.kjhxtc.crypto.params;

public class SM2KeyParameters extends ECKeyParameters {
    protected SM2KeyParameters(boolean flag,
                               ECDomainParameters ecdomainparameters) {
        super(flag, ecdomainparameters);
    }
}
