package com.kjhxtc.crypto.util;

public abstract interface Coder
{
  public abstract String encode(byte[] paramArrayOfByte);

  public abstract byte[] decode(String paramString);
}