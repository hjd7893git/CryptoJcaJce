package com.kjhxtc.crypto.util;

import com.kjhxtc.asn1.*;
import com.kjhxtc.asn1.cngmt.GMTObjectIdentifiers;
import com.kjhxtc.asn1.pkcs.PKCSObjectIdentifiers;
import com.kjhxtc.asn1.pkcs.RSAPublicKey;
import com.kjhxtc.asn1.x509.AlgorithmIdentifier;
import com.kjhxtc.asn1.x509.SubjectPublicKeyInfo;
import com.kjhxtc.asn1.x9.X962Parameters;
import com.kjhxtc.asn1.x9.X9ECParameters;
import com.kjhxtc.asn1.x9.X9ECPoint;
import com.kjhxtc.asn1.x9.X9ObjectIdentifiers;
import com.kjhxtc.crypto.params.*;

import java.io.IOException;

/**
 * Factory to create ASN.1 subject public key info objects from lightweight public keys.
 */
public class SubjectPublicKeyInfoFactory
{
    /**
     * Create a SubjectPublicKeyInfo public key.
     *
     * @param publicKey the SubjectPublicKeyInfo encoding
     * @return the appropriate key parameter
     * @throws java.io.IOException on an error encoding the key
     */
    public static SubjectPublicKeyInfo createSubjectPublicKeyInfo(AsymmetricKeyParameter publicKey) throws IOException
    {
        if (publicKey instanceof RSAKeyParameters)
        {
            RSAKeyParameters pub = (RSAKeyParameters)publicKey;

            return new SubjectPublicKeyInfo(new AlgorithmIdentifier(PKCSObjectIdentifiers.rsaEncryption, DERNull.INSTANCE), new RSAPublicKey(pub.getModulus(), pub.getExponent()));
        }
        else if (publicKey instanceof DSAPublicKeyParameters)
        {
            DSAPublicKeyParameters pub = (DSAPublicKeyParameters)publicKey;

            return new SubjectPublicKeyInfo(new AlgorithmIdentifier(X9ObjectIdentifiers.id_dsa), new ASN1Integer(pub.getY()));
        }
        else if (publicKey instanceof ECPublicKeyParameters)
        {
            ECPublicKeyParameters pub = (ECPublicKeyParameters)publicKey;
            ECDomainParameters domainParams = pub.getParameters();
            ASN1Encodable      params;

            if (domainParams == null)
            {
                params = new X962Parameters(DERNull.INSTANCE);      // Implicitly CA
            }
            else if (domainParams instanceof ECNamedDomainParameters)
            {
                params = new X962Parameters(((ECNamedDomainParameters)domainParams).getName());
            }
            else
            {
                X9ECParameters ecP = new X9ECParameters(
                    domainParams.getCurve(),
                    domainParams.getG(),
                    domainParams.getN(),
                    domainParams.getH(),
                    domainParams.getSeed());

                params = new X962Parameters(ecP);
            }

            ASN1OctetString p = (ASN1OctetString)new X9ECPoint(pub.getQ()).toASN1Primitive();

            return new SubjectPublicKeyInfo(new AlgorithmIdentifier(X9ObjectIdentifiers.id_ecPublicKey, params), p.getOctets());
        }
        else if (publicKey instanceof SM2PublicKeyParameters) {
            /*
             * @see <a href="http://www.gmbz.org.cn/main/viewfile/20180110015355638195.html">GM/T 0009-2012</a>
             */
            ASN1EncodableVector v = new ASN1EncodableVector();
            v.add(X9ObjectIdentifiers.id_ecPublicKey);
            v.add(GMTObjectIdentifiers.id_sm2);
            return new SubjectPublicKeyInfo(
                    AlgorithmIdentifier.getInstance(new DERSequence(v)),
                    ((SM2PublicKeyParameters) publicKey).getQ().getEncoded()
            );
        }
        else
        {
            throw new IOException("key parameters not recognised.");
        }
    }
}
