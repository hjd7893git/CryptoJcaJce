package com.kjhxtc.jce.interfaces;

import com.kjhxtc.jce.spec.ElGamalParameterSpec;

public interface ElGamalKey
{
    public ElGamalParameterSpec getParameters();
}
