package com.kjhxtc.asn1.smime;

import com.kjhxtc.asn1.DERSequence;
import com.kjhxtc.asn1.DERSet;
import com.kjhxtc.asn1.cms.Attribute;

public class SMIMECapabilitiesAttribute
    extends Attribute
{
    public SMIMECapabilitiesAttribute(
        SMIMECapabilityVector capabilities)
    {
        super(SMIMEAttributes.smimeCapabilities,
                new DERSet(new DERSequence(capabilities.toASN1EncodableVector())));
    }
}
