package com.kjhxtc.asn1.cngmt;

import com.kjhxtc.asn1.ASN1ObjectIdentifier;

/**
 * GM/T 0006-2012 附录标准OID
 *
 * @see <a href="http://www.gmbz.org.cn/main/viewfile/20180108024017966885.html">GM/T 0006-2012 (PDF)</a>
 */
public interface GMTObjectIdentifiers {
    /*
     * 1.2.156.10197 密码行业标准化技术委员会
     */

    /**
     * 基础密码算法
     */
    static final ASN1ObjectIdentifier gmtAlgorithm = new ASN1ObjectIdentifier("1.2.156.10197.1");

    /* SM4(SMS4) */
    static final ASN1ObjectIdentifier id_sm4 = gmtAlgorithm.branch("104");
    static final ASN1ObjectIdentifier id_sm4_ecb = id_sm4.branch("1");
    static final ASN1ObjectIdentifier id_sm4_cbc = id_sm4.branch("2");
    static final ASN1ObjectIdentifier id_sm4_ofb = id_sm4.branch("3");
    static final ASN1ObjectIdentifier id_sm4_cfb = id_sm4.branch("4");

    /* SM2(ECC) */
    static final ASN1ObjectIdentifier id_sm2 = gmtAlgorithm.branch("301");
    static final ASN1ObjectIdentifier id_ecc_sm2_sign = id_sm2.branch("1");
    static final ASN1ObjectIdentifier id_ecc_sm2_keyAgreement = id_sm2.branch("2");
    static final ASN1ObjectIdentifier id_ecc_sm2_encrypt = id_sm2.branch("3");

    /* SM3杂凑算法 */
    static final ASN1ObjectIdentifier id_sm3 = gmtAlgorithm.branch("401");
    static final ASN1ObjectIdentifier id_sm3_basic = id_sm3.branch("1");
    static final ASN1ObjectIdentifier id_sm3_secKey = id_sm3.branch("2");

    // SM3withSM2
    static final ASN1ObjectIdentifier sm3WithSM2 = gmtAlgorithm.branch("501");
    // SM3withRSA
    static final ASN1ObjectIdentifier sm3WithRSAEncryption = gmtAlgorithm.branch("504");

    /*
     * GM/T 0009-2012 SM2密码算法使用规范
     * GM/T 0010-2012 SM2密码算法加密签名消息语法规范
     */
    static final ASN1ObjectIdentifier gmt0010_2012 = new ASN1ObjectIdentifier("1.2.156.10197.6.1.4.2");
    static final ASN1ObjectIdentifier data = gmt0010_2012.branch("1");
    static final ASN1ObjectIdentifier signedData = gmt0010_2012.branch("2");
    static final ASN1ObjectIdentifier envelopedData = gmt0010_2012.branch("3");
    static final ASN1ObjectIdentifier signedAndEnvelopedData = gmt0010_2012.branch("4");
    static final ASN1ObjectIdentifier encryptedData = gmt0010_2012.branch("5");
    static final ASN1ObjectIdentifier keyAgreementInfo = gmt0010_2012.branch("6");

}