package com.kjhxtc.asn1.cngmt;

import com.kjhxtc.asn1.ASN1ObjectIdentifier;
import com.kjhxtc.asn1.x9.X9ECParameters;
import com.kjhxtc.asn1.x9.X9ECParametersHolder;
import com.kjhxtc.crypto.params.ECDomainParameters;
import com.kjhxtc.crypto.util.encoder.Hex;
import com.kjhxtc.math.ec.ECCurve;
import com.kjhxtc.util.Strings;

import java.math.BigInteger;
import java.util.Enumeration;
import java.util.Hashtable;

public class GMTNamedCurves
{
    /**
     * GMT 0003.5-2012
     *
     * @see <a href="http://www.gmbz.org.cn/main/viewfile/2018010802371372251.html">SM2 椭圆曲线公钥密码算法第5部分：参数定义</a>
     */
    public static final X9ECParametersHolder SM2p256v1Parameters = new X9ECParametersHolder()
    {
        private final String[] sm2_param = {
            /* Stand Recommend Parameter */
            "FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFF",// p,0
            "FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFC",// a,1
            "28E9FA9E9D9F5E344D5A9E4BCF6509A7F39789F515AB8F92DDBCBD414D940E93",// b,2
            "FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFF7203DF6B21C6052B53BBF40939D54123",// n,3
            "32C4AE2C1F1981195F9904466A39C9948FE30BBFF2660BE1715A4589334C74C7",// Gx,4
            "BC3736A2F4F6779C59BDCEE36B692153D0A9877CC62A474002DF32E52139F0A0" // Gy,5

            /* Test ECC Parameter */
/*
            "8542D69E4C044F18E8B92435BF6FF7DE457283915C45517D722EDB8B08F1DFC3",// p,0
            "787968B4FA32C3FD2417842E73BBFEFF2F3C848B6831D7E0EC65228B3937E498",// a,1
            "63E4C6D3B23B0C849CF84241484BFE48F61D59A5B16BA06E6E12D1DA27C5249A",// b,2
            "8542D69E4C044F18E8B92435BF6FF7DD297720630485628D5AE74EE7C32E79B7",// n,3
            "421DEBD61B62EAB6746434EBC3CC315E32220B3BADD50BDC4C4E6C147FEDD43D",// gx,4
            "0680512BCBB42C07D47349D2153B70C4E5D7FDFCBFA36EA1A85841B9E46E09A2" // gy,5
*/
        };
        private final BigInteger SM2_p = new BigInteger(sm2_param[0], 16);
        private final BigInteger SM2_a = new BigInteger(sm2_param[1], 16);
        private final BigInteger SM2_b = new BigInteger(sm2_param[2], 16);
        private final BigInteger SM2_n = new BigInteger(sm2_param[3], 16);

        private final ECCurve.Fp Curve = new ECCurve.Fp(
                SM2_p, // q
                SM2_a, // a
                SM2_b  // b
        );

        private final ECDomainParameters Params = new ECDomainParameters(
                Curve,                                                             // p,a,b
                Curve.decodePoint(Hex.decode("04" + sm2_param[4] + sm2_param[5])), // G  格式+Gx+Gy
                SM2_n                                                              // n
        );

        protected X9ECParameters createParameters()
        {
            return new X9ECParameters(Curve, Params.getG(), SM2_n);
        }
    };

    static final Hashtable objIds = new Hashtable();
    static final Hashtable curves = new Hashtable();
    static final Hashtable names = new Hashtable();

    static void defineCurve(String name, ASN1ObjectIdentifier oid, X9ECParametersHolder holder)
    {
        objIds.put(name, oid);
        names.put(oid, name);
        curves.put(oid, holder);
    }

    static
    {
        defineCurve("sm2p256v1", GMTObjectIdentifiers.id_sm2, SM2p256v1Parameters);
//        defineCurveWithOID("sm2p256v1", GMTObjectIdentifiers.id_sm2, GMTNamedCurves.SM2p256v1Parameters);
//        defineCurveWithOID("sm2p256v1", GMTObjectIdentifiers.sm3WithSM2, GMTNamedCurves.SM2p256v1Parameters);
//        defineCurveWithOID("sm2p256v1", GMTObjectIdentifiers.id_ecc_sm2_encrypt, GMTNamedCurves.SM2p256v1Parameters);
//        defineCurveWithOID("sm2p256v1", GMTObjectIdentifiers.id_ecc_sm2_sign, GMTNamedCurves.SM2p256v1Parameters);
//
//        defineCurveAlias("SM2", GMTObjectIdentifiers.id_sm2);
    }

    public static X9ECParameters getByName(
            String name)
    {
        ASN1ObjectIdentifier oid = (ASN1ObjectIdentifier)objIds.get(Strings.toLowerCase(name));

        if (oid != null)
        {
            return getByOID(oid);
        }

        return null;
    }

    /**
     * return the X9ECParameters object for the named curve represented by
     * the passed in object identifier. Null if the curve isn't present.
     *
     * @param oid an object identifier representing a named curve, if present.
     */
    public static X9ECParameters getByOID(
            ASN1ObjectIdentifier oid)
    {
        X9ECParametersHolder holder = (X9ECParametersHolder)curves.get(oid);

        if (holder != null)
        {
            return holder.getParameters();
        }

        return null;
    }

    /**
     * return the object identifier signified by the passed in name. Null
     * if there is no object identifier associated with name.
     *
     * @return the object identifier associated with name, if present.
     */
    public static ASN1ObjectIdentifier getOID(
            String name)
    {
        return (ASN1ObjectIdentifier)objIds.get(Strings.toLowerCase(name));
    }

    /**
     * return the named curve name represented by the given object identifier.
     */
    public static String getName(
            ASN1ObjectIdentifier oid)
    {
        return (String)names.get(oid);
    }

    /**
     * returns an enumeration containing the name strings for curves
     * contained in this structure.
     */
    public static Enumeration getNames()
    {
        return objIds.keys();
    }
}
