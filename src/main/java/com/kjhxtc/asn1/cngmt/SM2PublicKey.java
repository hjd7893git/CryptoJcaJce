package com.kjhxtc.asn1.cngmt;

import com.kjhxtc.asn1.ASN1Object;
import com.kjhxtc.asn1.ASN1Primitive;
import com.kjhxtc.asn1.DERBitString;
import com.kjhxtc.util.Arrays;
import com.kjhxtc.util.BigIntegers;
import com.kjhxtc.util.encoders.Hex;

import java.io.IOException;
import java.math.BigInteger;
import java.security.PublicKey;

public class SM2PublicKey extends ASN1Object {

    private final byte[] publicKeyData;

    public SM2PublicKey(PublicKey publicKey) {
        this(publicKey.getEncoded());
    }

    /**
     * 快捷构造（直接使用坐标构造出公钥编码数据）
     *
     * @param x 公钥坐标XCoordinate
     * @param y 公钥坐标YCoordinate
     */
    public SM2PublicKey(BigInteger x, BigInteger y) {
        this(Arrays.concatenate(new byte[]{0x04},
                                BigIntegers.asUnsignedByteArray(32, x),
                                BigIntegers.asUnsignedByteArray(32, y))
        );
    }

    private SM2PublicKey(byte[] publicKey) {
        if (null == publicKey || publicKey.length < 65 || publicKey[0] != 0x04) {
            throw new IllegalArgumentException("Invalid PublicKey Data");
        }
        publicKeyData = publicKey.clone();
    }

    public static SM2PublicKey fromByteArray(byte[] array) {
        try {
            ASN1Primitive obj = DERBitString.fromByteArray(array);
            if (obj instanceof DERBitString) {
                return new SM2PublicKey(((DERBitString) obj).getBytes());
            } else {
                System.out.println(obj.getClass());
                throw new IllegalArgumentException("Invalid SM2PublicKey");
            }
        } catch (IOException e) {
            throw new IllegalArgumentException("Invalid SM2PublicKey: ASN.1 Parse Error");
        }
    }

    public static SM2PublicKey getInstance(Object obj) {
        if (obj instanceof SM2PublicKey) {
            return (SM2PublicKey) obj;
        } else if (obj instanceof byte[]) { // DER BIT STRING
            return SM2PublicKey.fromByteArray((byte[]) obj);
        } else if (obj != null) {
            byte[] publicKeyData = DERBitString.getInstance(obj).getBytes();
            return new SM2PublicKey(publicKeyData);
        }

        return null;
    }

    /**
     * @return SM2 公钥编码值 04||X||Y
     */
    public byte[] getPublicKeyData()
    {
        return publicKeyData;
    }

    @Override
    public String toString() {
        try {
            return "#" + Hex.toHexString(new DERBitString(publicKeyData).getEncoded()).toUpperCase();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * GMT 0009-2012
     * <p>
     * SM2 算法公钥数据格式的 ASN.1 定义为:
     * <pre>
     *    SM2PublicKey :: = BITSTRING
     * </pre>
     * SM2PublicKey 为 BITSTRING 类型, 内容为 04‖X‖Y , 其中,
     * X 和 Y 分别标识公钥的 x 分量和 y
     * 分量, 其长度各为 256 位。
     */
    public ASN1Primitive toASN1Primitive() {
        return new DERBitString(publicKeyData);
    }
}
