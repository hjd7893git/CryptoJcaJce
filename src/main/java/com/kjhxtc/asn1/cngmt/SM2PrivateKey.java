package com.kjhxtc.asn1.cngmt;

import com.kjhxtc.asn1.ASN1Integer;
import com.kjhxtc.asn1.ASN1Object;
import com.kjhxtc.asn1.ASN1Primitive;

import java.math.BigInteger;

public class SM2PrivateKey extends ASN1Object {
    private final ASN1Integer d;

    SM2PrivateKey(BigInteger d) {
        this.d = new ASN1Integer(d);
    }

    public static SM2PrivateKey fromBigInteger(BigInteger d) {
        return new SM2PrivateKey(d);
    }

    public static SM2PrivateKey getInstance(
            Object obj) {
        if (obj instanceof SM2PrivateKey) {
            return (SM2PrivateKey) obj;
        }

        if (obj != null) {
            return new SM2PrivateKey(ASN1Integer.getInstance(obj).getValue());
        }

        return null;
    }

    public BigInteger getKey(){
        return d.getValue();
    }

    /**
     * GMT 0009-2012
     * <pre>
     *    SM2PrivateKey  ::= INTEGER
     *    SM2PublicKey   ::= BITSTRING
     * </pre>
     * GMT 0010-2012 (same as X962)
     * <pre>
     *  Parameters    ::= CHOICE {
     *      ecParameters        ECParameters,
     *      namedCurve          ObjectIdentifier,
     *      implicitlyCA        NULL
     *  }
     *
     *  ECPrivateKey   ::= SEQUENCE{
     *     version              INTEGER { ecPrivkeyVer1(1) }(ecPrivkeyVer1),
     *     privateKey           SM2PrivateKey,
     *     parameters [0]       Parameters{{IOSet}} OPTIONAL,
     *     publicKey  [1]       SM2PublicKey
     *  }
     * </pre>
     * <p>
     * {@link com.kjhxtc.asn1.x9.X962Parameters#toASN1Primitive ECParameters}
     */
    public ASN1Primitive toASN1Primitive() {
        return d;
    }
}
