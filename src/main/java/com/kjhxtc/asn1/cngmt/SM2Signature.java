package com.kjhxtc.asn1.cngmt;

import com.kjhxtc.asn1.*;

import java.math.BigInteger;

public class SM2Signature
        extends ASN1Object {
    private ASN1Integer r;
    private ASN1Integer s;


    private void init(ASN1Integer r, ASN1Integer s) {
        this.r = r;
        this.s = s;
    }

    public SM2Signature(
            BigInteger r, BigInteger s) {
        if (r == null || s == null)
            throw new IllegalArgumentException("R or S should be not NULL");
        init(new ASN1Integer(r), new ASN1Integer(s));
    }

    public BigInteger getR() {
        return r.getValue();
    }

    public BigInteger getS() {
        return s.getValue();
    }

    private SM2Signature(
            ASN1Sequence seq) {
        if (seq.size() != 2) {
            throw new IllegalArgumentException("Not SM2Signature value");
        }
        init((ASN1Integer) seq.getObjectAt(0).toASN1Primitive(),
             (ASN1Integer) seq.getObjectAt(1).toASN1Primitive());
    }

    public static SM2Signature getInstance(
            ASN1TaggedObject obj,
            boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }

    public static SM2Signature getInstance(
            Object obj) {
        if (obj instanceof SM2Signature) {
            return (SM2Signature) obj;
        } else if (obj != null) {
            return new SM2Signature(ASN1Sequence.getInstance(obj));
        }

        return null;
    }

    /**
     * Produce an object suitable for an ASN1OutputStream.
     * GMT 0009-2012
     * <pre>
     * SM2Signature  ::=  SEQUENCE {
     *     R              INTEGER,
     *     S              INTEGER
     * }
     * </pre>
     *
     * @see <a href="http://www.gmbz.org.cn/main/viewfile/2018011001400692565.html">GM/T 0009-2012</a>
     */
    public ASN1Primitive toASN1Primitive() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(r);
        v.add(s);
        return new DERSequence(v);
    }
}