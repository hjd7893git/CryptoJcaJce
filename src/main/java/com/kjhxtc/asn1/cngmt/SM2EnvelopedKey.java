package com.kjhxtc.asn1.cngmt;

import com.kjhxtc.asn1.*;
import com.kjhxtc.asn1.x509.AlgorithmIdentifier;

public class SM2EnvelopedKey extends ASN1Object {

    private AlgorithmIdentifier symAlgId;
    private SM2Cipher symEncryptKey;
    private SM2PublicKey sm2PublicKey;
    private DERBitString sm2EncryptedPrivateKey;

    SM2EnvelopedKey(ASN1Sequence instance) {
        if (instance.size() < 4) {
            throw new IllegalArgumentException("Invalid SM2EnvelopedKey");
        }
        symAlgId = AlgorithmIdentifier.getInstance(instance.getObjectAt(0));
        symEncryptKey = SM2Cipher.getInstance(instance.getObjectAt(1));
        sm2PublicKey = SM2PublicKey.getInstance(instance.getObjectAt(2));
        sm2EncryptedPrivateKey = DERBitString.getInstance(instance.getObjectAt(3));
    }

    SM2EnvelopedKey(AlgorithmIdentifier identifier, SM2Cipher symEncryptKey, SM2PublicKey sm2PublicKey, byte[] encryptedKey) {
        this.symAlgId = identifier;
        this.symEncryptKey = symEncryptKey;
        this.sm2PublicKey = sm2PublicKey;
        this.sm2EncryptedPrivateKey = new DERBitString(encryptedKey);
    }

    public static SM2EnvelopedKey getInstance(AlgorithmIdentifier identifier, SM2Cipher symEncryptKey, SM2PublicKey sm2PublicKey, byte[] encryptedKey) {
        return new SM2EnvelopedKey(identifier, symEncryptKey, sm2PublicKey, encryptedKey);
    }

    public static SM2EnvelopedKey getInstance(Object obj) {
        if (obj instanceof SM2EnvelopedKey) {
            return (SM2EnvelopedKey) obj;
        } else if (obj != null) {
            return new SM2EnvelopedKey(ASN1Sequence.getInstance(obj));
        }
        return null;
    }

    public AlgorithmIdentifier getSymAlgId() {
        return symAlgId;
    }

    public DERBitString getSm2EncryptedPrivateKey() {
        return sm2EncryptedPrivateKey;
    }

    public SM2Cipher getSymEncryptKey() {
        return symEncryptKey;
    }

    public SM2PublicKey getSm2PublicKey() {
        return sm2PublicKey;
    }

    /**
     * Produce an object suitable for an ASN1OutputStream.
     * GMT 0009-2012
     * <pre>
     * SM2EnvelopedKey  ::=  SEQUENCE {
     *     symAlgId                       AlgorithmIdentifier,  -- 对称密钥算法标识
     *     symEncryptKey                  SM2Cipher,            -- 对称密钥密文
     *     Sm2PublicKey                   SM2PublicKey,         -- SM2 公钥
     *     Sm2EncryptedPrivateKey         BitString,            -- SM2 私钥密文
     * }
     * </pre>
     *
     * @see <a href="http://www.gmbz.org.cn/main/viewfile/2018011001400692565.html">GM/T 0009-2012</a>
     */
    public ASN1Primitive toASN1Primitive() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(symAlgId);
        v.add(symEncryptKey);
        v.add(sm2PublicKey);
        v.add(sm2EncryptedPrivateKey);
        return new DERSequence(v);
    }
}
