package com.kjhxtc.asn1.cngmt;

import com.kjhxtc.asn1.*;
import com.kjhxtc.util.encoders.Hex;

import java.io.IOException;
import java.math.BigInteger;

public class SM2Cipher
        extends ASN1Object {
    private ASN1Integer x;
    private ASN1Integer y;
    private ASN1OctetString hash;
    private ASN1OctetString cipherText;


    /**
     * 构建SM2-ASN1结构数据
     *
     * @param x          临时坐标
     * @param y          临时坐标
     * @param hash       摘要
     * @param cipherText 消息密文
     */
    public SM2Cipher(BigInteger x, BigInteger y, byte[] hash, byte[] cipherText) {
        init(new ASN1Integer(x), new ASN1Integer(y),
             new DEROctetString(hash), new DEROctetString(cipherText));
    }

    private SM2Cipher(
            ASN1Sequence seq) {
        if (seq.size() != 4) {
            throw new IllegalArgumentException("Not SM2Cipher value");
        }
        init((ASN1Integer) seq.getObjectAt(0).toASN1Primitive(),
             (ASN1Integer) seq.getObjectAt(1).toASN1Primitive(),
             (ASN1OctetString) seq.getObjectAt(2).toASN1Primitive(),
             (ASN1OctetString) seq.getObjectAt(3).toASN1Primitive()
        );
    }

    public static SM2Cipher getInstance(
            ASN1TaggedObject obj,
            boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }

    public static SM2Cipher getInstance(
            Object obj) {
        if (obj instanceof SM2Cipher) {
            return (SM2Cipher) obj;
        } else if (obj != null) {
            return new SM2Cipher(ASN1Sequence.getInstance(obj));
        }

        return null;
    }

    private void init(ASN1Integer x, ASN1Integer y, ASN1OctetString hash, ASN1OctetString cipherText) {
        if (null == hash || 32 != hash.getOctets().length) {
            throw new IllegalArgumentException("Invalid hash, must be 32 Bytes");
        }
        if (null == cipherText || cipherText.getOctets().length < 1) {
            throw new IllegalArgumentException("Invalid cipherText");
        }
        this.x = x;
        this.y = y;
        this.hash = hash;
        this.cipherText = cipherText;
    }

    public byte[] getCipherText() {
        return cipherText.getOctets();
    }

    public byte[] getDigest() {
        return hash.getOctets();
    }

    public BigInteger getXCoordinate() {
        return x.getValue();
    }

    public BigInteger getYCoordinate() {
        return y.getValue();
    }

    /**
     * Produce an object suitable for an ASN1OutputStream.
     * GMT 0009-2012 #7.2
     * <pre>
     * SM2Cipher  ::=  SEQUENCE {
     *     XCoordinate      INTEGER,               -- X分量
     *     YCoordinate      INTEGER,               -- Y分量
     *     HASH             OCTET STRING SIZE(32), -- 杂凑值
     *     CipherText       OCTET STRING            --密文
     * }
     * </pre>
     *
     * @see <a href="http://www.gmbz.org.cn/main/viewfile/2018011001400692565.html">GM/T 0009-2012</a>
     */
    public ASN1Primitive toASN1Primitive() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(x);
        v.add(y);
        v.add(hash);
        v.add(cipherText);
        return new DERSequence(v);
    }

    @Override
    public String toString() {
        try {
            return "#" + Hex.toHexString(toASN1Primitive().getEncoded("DER")).toUpperCase();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}