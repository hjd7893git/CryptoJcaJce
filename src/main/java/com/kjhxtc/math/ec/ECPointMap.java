package com.kjhxtc.math.ec;

public interface ECPointMap
{
    ECPoint map(ECPoint p);
}
