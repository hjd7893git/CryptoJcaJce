package com.kjhxtc.math.ec.endo;

import com.kjhxtc.math.ec.ECPointMap;

public interface ECEndomorphism
{
    ECPointMap getPointMap();

    boolean hasEfficientPointMap();
}
