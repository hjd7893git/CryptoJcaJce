package com.kjhxtc.pqc.crypto.gmss;

import com.kjhxtc.crypto.Digest;

public interface GMSSDigestProvider
{
    Digest get();
}
