package com.kjhxtc.util;

public interface Selector
    extends Cloneable
{
    boolean match(Object obj);

    Object clone();
}
